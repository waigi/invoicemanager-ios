//
//  FileManagerTests.swift
//  InvoiceManagerTests
//
//  Created by Can Zhan on 12/9/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import XCTest
@testable import InvoiceManager

class FileManagerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testHomeDirectory() {
        print(FileUtil.homeDirectory?.path ?? "")
        XCTAssert(FileUtil.homeDirectory != nil)
    }
    
    func testCreateDirectory() {
        guard let path = FileUtil.createDirectory(for: Date())?.path else {
            XCTFail("url is nil")
            return
        }
        print(path)
        XCTAssert(path != "")
        var isDirectory: ObjCBool = false
        XCTAssert(FileManager.default.fileExists(atPath: path, isDirectory: &isDirectory))
        XCTAssert(isDirectory.boolValue)
    }
    
    func testMoveFile() {
        let tmpFileUrl = FileManager.default.temporaryDirectory.appendingPathComponent("tmp.png")
        FileManager.default.createFile(atPath: tmpFileUrl.path, contents: nil)
        
        var isDirectory: ObjCBool = false
        XCTAssert(FileManager.default.fileExists(atPath: tmpFileUrl.path, isDirectory: &isDirectory))
        XCTAssertFalse(isDirectory.boolValue)
        
        guard let destFileUrl = FileUtil.moveFile(at: tmpFileUrl, to: Date()) else {
            XCTFail("not file is nil")
            return
        }
        
        XCTAssertFalse(FileManager.default.fileExists(atPath: tmpFileUrl.path))
        XCTAssert(FileManager.default.fileExists(atPath: destFileUrl.path, isDirectory: &isDirectory))
        XCTAssertFalse(isDirectory.boolValue)
        print(destFileUrl)
        
        try? FileManager.default.removeItem(at: destFileUrl)
        XCTAssertFalse(FileManager.default.fileExists(atPath: destFileUrl.path))
    }
    
    func testExport() {
        let url1 = FileUtil.exportInvoicesCSV()
        XCTAssertNotNil(url1)
        let url2 = FileUtil.exportInvoicesHTML()
        XCTAssertNotNil(url2)
    }
    
    func testExportAndZip() {
        FileUtil.exportAndZipInvoices(progress: { (progress) in
            print("Progress = \(progress)")
        }, success: { (url) in
            print("Successfully exported and zipped into \(url)")
            XCTAssertTrue(true)
        }) {
            XCTFail("Failed to export and zip")
        }
    }
    
    func testDownloadFile() {
        FileUtil.exportAndZipInvoices(progress: { (progress) in
            print("Progress = \(progress)")
        }, success: { (url) in
            print("Successfully exported and zipped into \(url)")
            WebServer.filePath = url.path
            WebServer.start()
            let expectation = XCTestExpectation(description: "web accessed")
            expectation.fulfill()   // comment out this line for testing via browser
            wait(for: [expectation], timeout: 600)
        }) {
            XCTFail("Failed to export and zip")
        }
    }
}
