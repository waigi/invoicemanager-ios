//
//  StringTest.swift
//  InvoiceManagerTests
//
//  Created by Can Zhan on 8/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import XCTest
@testable import InvoiceManager

class StringTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testABNDetection() {
        let s1 = "ABN 440 23 123 123"
        let s2 = "abn23453829463"
        let s3 = "A b n 39 234 234 234"
        let s4 = "39 234 234 234"
        let s5 = "BN 440 23 123 123"
        let s6 = "A b n no 12 213 123 123"
        let s7 = " A. b. n.: 12 213 123 123 "
        let s8 = """
                Good Call2212 Dee Why ABN: 31 610 667 831REG:1 TRAN:63585014/11/2018 20:52:44 ST:2212BTAX Tax DescriptionINVOICE QtyAmointPULP 98No.615.97 L $1.479/ L $23.62Total: AMEX: Change:$23.62 $23.62 $0.00ANZ EFTPOS ANZ CUSTOMER COPY 7-ELEVEN 2212 940 PITTWATER RD DEE WHY TERMINAL ID 01422221201AMEX 1006 PUR(C) CR $23.62 A000000025010801AUDAMERICAN EXPRESSAID TVR0000008000APSN 00 PO0007 ATC 0052 APPROVED AUTH: 851990STAN: 129092 RRN: 14/11/18 20:5201300283069EFTPOS FROM ANZ$2.15Total inc ludes GST of:Indicates items with GST
                """
        XCTAssertEqual(s1.abn, "44023123123")
        XCTAssertEqual(s2.abn, "23453829463")
        XCTAssertEqual(s3.abn, "39234234234")
        XCTAssertNil(s4.abn)
        XCTAssertNil(s5.abn)
        XCTAssertEqual(s6.abn, "12213123123")
        XCTAssertEqual(s7.abn, "12213123123")
        XCTAssertEqual(s8.abn, "31610667831")
    }

    func testABNValidation() {
        let s1 = "44023123123"
        let s2 = "abn23453829463"
        let s3 = "392342342341"
        let s4 = "3923423423"
        let s5 = "440 23 123 123"
        let s6 = " 12 213 123 123"
        let s7 = "12 213 123 123 "
        XCTAssert(s1.isABN)
        XCTAssertFalse(s2.isABN)
        XCTAssertFalse(s3.isABN)
        XCTAssertFalse(s4.isABN)
        XCTAssertFalse(s5.isABN)
        XCTAssertFalse(s6.isABN)
        XCTAssertFalse(s7.isABN)
    }
    
}
