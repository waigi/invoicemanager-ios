//
//  ArrayTest.swift
//  InvoiceManagerTests
//
//  Created by Can Zhan on 21/1/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import XCTest

class ArrayTest: XCTestCase {

    func testClosestElementTo() {
        let array1 = [Double]()
        XCTAssertNil(array1.closestElementTo(1))
        
        let array2: [Double] = [1.0]
        XCTAssertNil(array2.closestElementTo(0.99))
        XCTAssertEqual(array2.closestElementTo(1.0), 1.0)
        XCTAssertEqual(array2.closestElementTo(1.99), 1.0)
        
        let array3: [Double] = [1.0, 2.0]
        XCTAssertNil(array3.closestElementTo(0.99))
        XCTAssertEqual(array3.closestElementTo(1.0), 1.0)
        XCTAssertEqual(array3.closestElementTo(1.99), 1.0)
        XCTAssertEqual(array3.closestElementTo(2.0), 2.0)
        XCTAssertEqual(array3.closestElementTo(2.99), 2.0)
        
        let array4: [Double] = [-1.0, 2.0, 3.0]
        XCTAssertNil(array4.closestElementTo(-2.99))
        XCTAssertEqual(array4.closestElementTo(0.99), -1.0)
        XCTAssertEqual(array4.closestElementTo(2.99), 2.0)
        XCTAssertEqual(array4.closestElementTo(3.99), 3.0)
        XCTAssertEqual(array4.closestElementTo(-1.0), -1.0)
        XCTAssertEqual(array4.closestElementTo(2.0), 2.0)
        XCTAssertEqual(array4.closestElementTo(3.0), 3.0)
    }

    func testBinarySearch() {
        let array1 = [Double]()
        XCTAssertNil(array1.binarySearch(1))
        
        let array2: [Double] = [1.0]
        XCTAssertNil(array2.binarySearch(0.99))
        XCTAssertNil(array2.binarySearch(1.99))
        XCTAssertEqual(array2.binarySearch(1.0), 0)
        
        let array3: [Double] = [1.0, 2.0]
        XCTAssertNil(array3.binarySearch(0.99))
        XCTAssertNil(array3.binarySearch(2.99))
        XCTAssertEqual(array3.binarySearch(1.0), 0)
        XCTAssertEqual(array3.binarySearch(2.0), 1)
        
        let array4: [Double] = [-1.0, 2.0, 3.0]
        XCTAssertNil(array4.binarySearch(-2.99))
        XCTAssertNil(array4.binarySearch(0.99))
        XCTAssertNil(array4.binarySearch(2.99))
        XCTAssertNil(array4.binarySearch(3.99))
        XCTAssertEqual(array4.binarySearch(-1.0), 0)
        XCTAssertEqual(array4.binarySearch(2.0), 1)
        XCTAssertEqual(array4.binarySearch(3.0), 2)
    }
}
