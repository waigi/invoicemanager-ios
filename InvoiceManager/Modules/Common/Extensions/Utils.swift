import AVFoundation
import AudioToolbox

struct Utils {
    
    static func hasUserPermittedForCameraAccess(completion: @escaping (Bool) -> ()) {
        if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
            completion(true)
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (isGranted: Bool) in
                completion(isGranted)
            })
        }
    }
        
    static var player: AVAudioPlayer?
    
}
