//
//  String+Utils.swift
//  OnesiteiOS
//
//  Created by bayya abhilash on 20/07/16.
//  Copyright © 2016 Woolworths. All rights reserved.
//

import UIKit

public extension String {

    func removeSpecialCharsFromString() -> String {
        let okayChars: Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890.")
        return String(self.filter { okayChars.contains($0) })
    }

    var localized: String {
        return NSLocalizedString(self, comment: "")
    }

    func localizedWithComment(comment: String) -> String {
        return NSLocalizedString(self, comment: comment)
    }

    /// Convert String to Barcode Image
    var barcodeImage: UIImage? {
        let data = self.data(using: .ascii)
        if let filter = CIFilter(name: "CICode128BarcodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 5, y: 5)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    /// Convert String to QR Code Image
    var qrcodeImage: UIImage? {
        let data = self.data(using: String.Encoding.ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 5, y: 5)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    /// If this string is a valid email address
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }

}

extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}

extension String {
    func truncate(length: Int, trailing: String = "…") -> String {
        return (self.count > length) ? self.prefix(length) + trailing : self
    }
}

extension NSAttributedString {
    func rightAlignedText() -> NSAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .right
        let mutableString = NSMutableAttributedString(attributedString: self)
        mutableString.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle],
                                    range: .init(location: 0, length: self.string.count))
        return mutableString
    }
}

// MARK: - Regex extraction from OCR detected text

// Regex patterns
extension String {
    
    /// Get 11 digits of ABC number from string
    var abn: String? {
        let expression = try? NSRegularExpression(pattern: RegexPattern.abn)
        if let range = expression?.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.count))?.range {
            let startIndex = self.index(self.startIndex, offsetBy: range.lowerBound)
            let endIndex = self.index(self.startIndex, offsetBy: range.upperBound)
            let trimmedString = String(self[startIndex..<endIndex]).replacingOccurrences(of: " ", with: "").trimmingCharacters(in: .whitespacesAndNewlines)
            let endIndexOfDigits = trimmedString.endIndex
            let startIndexOfDigits = trimmedString.index(trimmedString.endIndex, offsetBy: -11)
            return String(trimmedString[startIndexOfDigits..<endIndexOfDigits])
        }
        return nil
    }
    
    /// If the string is a valid ABN
    var isABN: Bool {
        let regex = RegexPattern.validABN
        let test = NSPredicate(format: "SELF MATCHES %@", regex)
        return test.evaluate(with: self)
    }
    
    /// A wrapper tuple type of recoginzed total and GST amounts
    typealias TotalWithGST = (total: Double, gst: Double)
    
    /// Find most possible Total Amount and GST Amount from recognized content string of an invoice
    var totalWithGST: TotalWithGST {
        var total = 0.0, gst = 0.0
        let expression = try? NSRegularExpression(pattern: RegexPattern.moneyAmount)
        if let matches = expression?.matches(in: self, range: NSRange(location: 0, length: self.count)),
            !matches.isEmpty {
            let amounts = matches.compactMap { (result: NSTextCheckingResult) -> Double? in
                let startIndex = self.index(self.startIndex, offsetBy: result.range.lowerBound)
                let endIndex = self.index(self.startIndex, offsetBy: result.range.upperBound)
                return Double(self[startIndex..<endIndex])
            }.sorted()
            if let largestAmount = amounts.last {
                // TODO: Set the largest amount as Total amount. It might be optimised by considering words in front of the amount, e.g. 'Saved', 'Saving', 'Cash', 'EFT' etc.
                total = largestAmount
                // TODO: Find the closest amount to the 1/11 of the total amount. In case there are GST free products, the actual GST amount can be less than some single product prices or even $0.00.
                gst = amounts.closestElementTo(total / 11.0) ?? 0.0
            }
        }
        return (total, gst)
    }
    
}
