//
//  UserDefaults+Extension.swift
//  InvoiceManager
//
//  Created by Can Zhan on 1/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

extension UserDefaults {
    
    private enum Keys: String {
        case defaultInvoiceDate
        case defaultCategory
    }
    
    /// Default invoice date when user captures a new photo
    static var defaultInvoiceDate: Date {
        get {
            return (UserDefaults.standard.value(forKey: Keys.defaultInvoiceDate.rawValue) as? Date) ?? Date()
        }
        set {
            UserDefaults.standard.set(newValue, forKey: Keys.defaultInvoiceDate.rawValue)
        }
    }
    
    /// Default invoice category when user captures a new photo
    static var defaultCategory: InvoiceCategory {
        get {
            if let rawValue = UserDefaults.standard.string(forKey: Keys.defaultCategory.rawValue), let category = InvoiceCategory(rawValue: rawValue) {
                return category
            } else {
                return .cashPayment
            }
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: Keys.defaultCategory.rawValue)
        }
    }
}
