//
//  Array+Extension.swift
//  InvoiceManager
//
//  Created by Can Zhan on 21/1/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation

extension Array where Element: Comparable {
    
    /// The closest element in the array to the ceil, but not greater than ceil
    public func closestElementTo(_ ceil: Element) -> Element? {
        guard !isEmpty else { return nil }
        var e: Element?
        var left = 0, right = count - 1
        while left <= right {
            let mid = (left + right) / 2
            if self[mid] == ceil {
                e = ceil
                break
            } else if self[mid] > ceil {
                right = mid - 1
            } else if self[mid] < ceil {
                if (mid + 1 < count && self[mid + 1] > ceil) || (mid == count - 1) {
                    e = self[mid]
                    break
                } else {
                    left = mid + 1
                }
            }
        }
        return e
    }
    
    /// Binary search the target's index in a sorted ascending array
    public func binarySearch(_ target: Element) -> Int? {
        guard !isEmpty else { return nil }
        var e: Int?
        var left = 0, right = count - 1
        while left <= right {
            let mid = (left + right) / 2
            if self[mid] == target {
                e = mid
                break
            } else if self[mid] < target {
                left = mid + 1
            } else if self[mid] > target {
                right = mid - 1
            }
        }
        return e
    }
    
}
