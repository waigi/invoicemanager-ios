//
//  Date+Extension.swift
//  ScanAndGo
//
//  Created by Can Zhan on 15/8/18.
//  Copyright © 2018 WOW. All rights reserved.
//

import Foundation

extension Date {
    
    /// Formatted date string with the pattern of "EEEE d MMMM yyyy". e.g. "Thursday 31 May 2018"
    var formattedLongDateString: String {
        return DateFormatter.longDateFormatter.string(from: self)
    }
    
    /// Formatter date string with the pattern of "HH:mm". e.g. "15:31"
    var formattedTimeString: String {
        return DateFormatter.timeFormatter.string(from: self)
    }
    
    /// Formatter date string with the pattern of "MMM, YYYY". e.g. "Apr, 2018"
    var formattedMonthYearString: String {
        return DateFormatter.monthYearFormatter.string(from: self)
    }
    
    var startDateOfTheMonth:Date {
        let currentDateComponents = Calendar.current.dateComponents([.year, .month], from: self)
        // Calculate the first date from the given date (Ex:- Given date is 27/08/2018 then first date of the month is 01/08/2018)
        return Calendar.current.date(from: currentDateComponents) ?? Date()
    }

    /// Formatter with the pattern of "HH:mm  dd/MM/yyyy". e.g. "14:10  15/02/2018"
    var formattedReceiptTransactionDateTimeString: String {
        return DateFormatter.timeAndShortDateFormatter.string(from: self)
    }
    
    /// Formatter with the pattern of "d MMMM yy HH:mm". e.g. "5 May 18 14:10"
    var formattedMediumDateAndTimeString: String {
        return DateFormatter.mediumDateAndTimeFormatter.string(from: self)
    }
    
    /// Formatter with the pattern of "dd MMM, HH:mm". e.g. "10 Aug, 7:49 PM"
    var transactionHistoryDateAndTimeString:String {
        return DateFormatter.transactionHistoryDateFormatter.string(from: self)
    }

    /// Formatter with the pattern of "MMM, YYYY". e.g. "Apr, 2018"
    var transactionHistorySectionDateAndTimeString:String {
        return DateFormatter.transactionHistorySectionDateFormatter.string(from: self)
    }
    
    /// Formatter with the pattern of "yyyy-MM-dd". e.g. "2018-09-12"
    var formattedYearMonthDateString: String {
        return DateFormatter.yearMonthDateFormatter.string(from: self)
    }

    /// Formatter with the pattern of "d MMM yyyy". e.g. "1 Nov 2018"
    var formattedDateMonthYearString: String {
        return DateFormatter.dateMonthYearDateFormatter.string(from: self)
    }
    
    /// Financial Year of the invoice in the format of "2017-2018"
    var financialYearString: String? {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: self)
        guard let year = components.year,
            let midYear = Calendar.current.date(from: DateComponents(year: year, month: 7, day: 1))
            else { return nil }
        if self < midYear {
            return "\(year - 1)-\(year)"
        } else {
            return "\(year)-\(year + 1)"
        }
    }
}
