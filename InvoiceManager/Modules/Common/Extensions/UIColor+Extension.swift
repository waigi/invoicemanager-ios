//
//  UIColor+Extension.swift
//  InvoiceManager
//
//  Created by Can Zhan on 18/9/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

extension UIColor {
    static let darkGreen = UIColor(red: 18.0/255.0, green: 84.0/255.0, blue: 48.0/255.0, alpha: 1.0)
    static let textGreen = UIColor(red: 0.0/255.0, green: 171.0/255.0, blue: 78.0/255.0, alpha: 1.0)
    
    static let textGray = UIColor(red: 69.0/255.0, green: 69.0/255.0, blue: 69.0/255.0, alpha: 1.0)
    static let lightGray = UIColor(red: 200.0/255.0, green: 199.0/255.0, blue: 204.0/255.0, alpha: 1.0)
    
    static let borderBlue = UIColor(red: 90.0/255.0, green: 183.0/255.0, blue: 234.0/255.0, alpha: 1.0)
    static let backgroundBlue = UIColor(red: 237.0/255.0, green: 246.0/255.0, blue: 251.0/255.0, alpha: 1.0)
    
    static let borderYellow = UIColor(red: 237.0/255.0, green: 192.0/255.0, blue: 10.0/255.0, alpha: 1.0)
    static let backgroundYellow = UIColor(red: 254.0/255.0, green: 249.0/255.0, blue: 235.0/255.0, alpha: 1.0)
    
    static let borderRed = UIColor(red: 255.0/255.0, green: 26.0/255.0, blue: 36.0/255.0, alpha: 1.0)
    static let backgroundRed = UIColor(red: 254.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    
    // MARK: - Cyan color palettes
    static let cyan50 = UIColor(named: "Cyan 50")!
    static let cyan100 = UIColor(named: "Cyan 100")!
    static let cyan200 = UIColor(named: "Cyan 200")!
    static let cyan300 = UIColor(named: "Cyan 300")!
    static let cyan500 = UIColor(named: "Cyan 500")!
    static let cyan600 = UIColor(named: "Cyan 600")!
    static let cyan700 = UIColor(named: "Cyan 700")!
    static let cyan900 = UIColor(named: "Cyan 900")!
    
    // MARK: - IMButton
    static let primaryButtonBackground = cyan500
    static let primaryButtonText = textGray
    static let secondaryButtonBackground = cyan200
    static let secondaryButtonText = textGray
    static let tertiaryButtonBackground = UIColor.clear
    static let tertiaryButtonText = textGray
}
