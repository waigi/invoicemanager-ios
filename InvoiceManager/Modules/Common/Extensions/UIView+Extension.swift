//
//  UIView+Extension.swift
//  ScanAndGo
//
//  Created by Can Zhan on 21/8/18.
//  Copyright © 2018 WOW. All rights reserved.
//

import UIKit


extension UIView {
    
    /// Apply border to view. Default to sngLightGray round border.
    func applyBorder(borderWidth: CGFloat = 1.0, borderColor: UIColor = .lightGray, cornerRadius: CGFloat = 5.0) {
        clipsToBounds = true
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.cornerRadius = cornerRadius
    }
    
    func applyShadow(shadowColor: UIColor = .lightGray,
                     opacity: Float = 1.0,
                     shadowRadius: CGFloat? = nil) {
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = .zero
        if let shadowRadius = shadowRadius {
            layer.shadowRadius = shadowRadius
        }
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    }
    
    /// Rotate view
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        layer.add(animation, forKey: nil)
    }
    
}
