//
//  DoubleExtension.swift
//  ScanAndGo
//
//  Created by Can Zhan on 28/6/18.
//  Copyright © 2018 WOW. All rights reserved.
//

import Foundation

extension Double {
    
    /// Formatted String with pattern "$%.2f" and using its absolute value. e.g. '1.0' -> '$1.00' or '-1.0' -> '$1.00'
    var dollarString: String {
        return String(format: "$%.2f", abs(self))
    }
    
    /// Formatted String with pattern "$%.2f" and using its raw value. e.g. '1.0' -> '$1.00' or '-1.0' -> '-$1.00'
    var rawDollarString: String {
        return self >= 0 ? String(format: "$%.2f", self) : String(format: "-$%.2f", abs(self))
    }
    
    /// Formatted String with pattern "%.2f" and using its raw value. e.g. '1.0' -> '1.00' or '-1.0' -> '-1.00'
    var string: String {
        return String(format: "%.2f", self)
    }
    
    // Moved from UITextField+Configuration.swift
    func splitAmountWithDollarsAndCents() -> (dollar: String, cents: String) {
        let stringAmount = String(format: "%.2f", self)
        let amountParts = stringAmount.components(separatedBy: ".")
        if amountParts.count > 1 {
            return (amountParts[0], amountParts[1])
        }
        else {
            return("0", "00")
        }
    }
    
}
