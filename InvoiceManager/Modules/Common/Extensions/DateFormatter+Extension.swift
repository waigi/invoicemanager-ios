//
//  DateFormatter+Extension.swift
//  ScanAndGo
//
//  Created by Can Zhan on 14/8/18.
//  Copyright © 2018 WOW. All rights reserved.
//

import Foundation

extension DateFormatter {
    
    /// Date formatter to parse API datetime. e.g. "2018-08-03T08:38:10.301Z"
    static let apiDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        return dateFormatter
    }()
    
    /// Date decoding strategy to decode API datetime. e.g. "2018-08-03T08:38:10.301Z"
    static let apiDateDecodingStrategy: JSONDecoder.DateDecodingStrategy = {
        return .formatted(DateFormatter.apiDateFormatter)
    }()
    
    /// Formatter with the pattern of "EEEE d MMMM yyyy". e.g. "Thursday 31 May 2018"
    static let longDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE d MMMM yyyy"
        return dateFormatter
    }()
    
    /// Formatter with the pattern of "HH:mm". e.g. "15:31"
    static let timeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter
    }()
    
    /// Formatter with the pattern of "MMM YYYY". e.g. "Apr 2018"
    static let monthYearFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM YYYY"
        return dateFormatter
    }()

    /// Formatter with the pattern of "dd MMM, HH:mm". e.g. "10 Aug, 7:49 PM"
    static let transactionHistoryDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, HH:mm"
        return dateFormatter
    }()
    
    /// Formatter with the pattern of "MMM, YYYY". e.g. "Apr, 2018"
    static let transactionHistorySectionDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM, YYYY"
        return dateFormatter
    }()

    /// Formatter with the pattern of "HH:mm  dd/MM/yyyy". e.g. "14:10  15/02/2018"
    static let timeAndShortDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm  dd/MM/yyyy"
        return dateFormatter
    }()
    
    /// Formatter with the pattern of "d MMMM yy HH:mm". e.g. "5 May 18 14:10"
    static let mediumDateAndTimeFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMMM yy HH:mm"
        return dateFormatter
    }()
    
    /// Formatter with the pattern of "yyyy-MM-dd". e.g. "2018-09-12"
    static let yearMonthDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    /// Formatter with the pattern of "d MMM yyyy". e.g. "1 Nov 2018"
    static let dateMonthYearDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM yyyy"
        return dateFormatter
    }()
}
