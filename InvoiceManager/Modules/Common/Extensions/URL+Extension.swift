//
//  URL+Extension.swift
//  InvoiceManager
//
//  Created by Can Zhan on 31/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation

extension URL {
    
    /// Home Directory related path, applicable to file path only
    var fileHomeDirectoryRelatedPath: String? {
        var pathString: String?
        guard let homePath = FileUtil.homeDirectory?.path else { return pathString }
        if path.hasPrefix(homePath) {
            pathString = String(path.dropFirst(homePath.count))
        }
        return pathString
    }
    
}
