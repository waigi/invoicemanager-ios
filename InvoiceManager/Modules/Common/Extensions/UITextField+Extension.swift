//
//  UITextField+Extension.swift
//  InvoiceManager
//
//  Created by Can Zhan on 13/12/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

extension UITextField {
    
    /// Show a Done button on keyboard, useful for Numpad, Decimal keyboards
    func applyKeyboardDoneButton() {
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(UITextField.tappedDoneButton))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolbar.items = [flexibleSpace, doneButton]
        toolbar.sizeToFit()
        inputAccessoryView = toolbar
    }
    
    @objc private func tappedDoneButton() {
        resignFirstResponder()
    }
}
