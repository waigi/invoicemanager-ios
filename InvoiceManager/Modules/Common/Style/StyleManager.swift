//
//  StyleManager.swift
//  InvoiceManager
//
//  Created by Can Zhan on 18/9/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

struct StyleManager {
    
    /// Generate an attributed string with given attributes
    /// - Parameter title: content string
    /// - Parameter weight: font weight. optional, default to .regular
    /// - Parameter size: font size. optional, default to 16
    /// - Parameter textColor: text color. optional, default to darkText
    static func attributedStringWith(title: String, weight: UIFont.Weight = .regular, size: CGFloat = 16, textColor: UIColor = .darkText) -> NSAttributedString {
        let attributes:[NSAttributedString.Key : Any] = [
            .font : UIFont.systemFont(ofSize: size, weight: .regular),
            .foregroundColor : textColor
        ]
        return  NSAttributedString(string: title, attributes: attributes)
    }
    
}
