//
//  InfoBarMessage.swift
//  ScanAndGo
//
//  Created by Can Zhan on 8/8/18.
//  Copyright © 2018 WOW. All rights reserved.
//

import Foundation

/// The Info bar has two major categories: Message and Toast
/// Message: There might be title or title and content. Texts are left aligned. e.g. Tender page.
/// Toast: There is content only. Texts are center aligned. e.g. PostScan screen
enum InfoBarMessageType {
    /// Blue, Left aligned, Title only or Title with Content
    case infoMessage
    /// Yellow, Left aligned, Title only or Title with Content
    case warningMessage
    /// Red, Left aligned, Title only or Title with Content
    case errorMessage
    /// Blue, Center aligned, Content only
    case infoToast
    /// Yellow, Center aligned, Content only
    case warningToast
    /// Red, Center aligned, Content only
    case errorToast
}

/// The message displayed with top Info Bar
struct InfoBarMessage {
    
    // MARK: - Vars
    
    /// Style of info bar.
    let type: InfoBarMessageType
    /// Message title. If ignored, the info bar icon is invisible
    let title: String?
    /// Message content.
    let content: String?
    
    // MARK: - Init
    
    private init(type: InfoBarMessageType, title: String?, content: String?) {
        self.type = type
        self.title = title
        self.content = content
    }
    
    // MARK: - Convenient Builders
    
    /// Build a .infoMessage type of InfoBarMessage
    static func infoMessageWith(_ title: String, _ content: String? = nil) -> InfoBarMessage {
        return InfoBarMessage(type: .infoMessage, title: title, content: content)
    }
    
    /// Build a .warningMessage type of InfoBarMessage
    static func warningMessageWith(_ title: String, _ content: String? = nil) -> InfoBarMessage {
        return InfoBarMessage(type: .warningMessage, title: title, content: content)
    }
    
    /// Build a .errorMessage type of InfoBarMessage
    static func errorMessageWith(_ title: String, _ content: String? = nil) -> InfoBarMessage {
        return InfoBarMessage(type: .errorMessage, title: title, content: content)
    }
    
    /// Build a .infoToast type of InfoBarMessage
    static func infoToastWith(_ content: String) -> InfoBarMessage {
        return InfoBarMessage(type: .infoToast, title: nil, content: content)
    }
    
    /// Build a .warningToast type of InfoBarMessage
    static func warningToastWith(_ content: String) -> InfoBarMessage {
        return InfoBarMessage(type: .warningToast, title: nil, content: content)
    }
    
    /// Build a .errorToast type of InfoBarMessage
    static func errorToastWith(_ content: String) -> InfoBarMessage {
        return InfoBarMessage(type: .errorToast, title: nil, content: content)
    }
}
