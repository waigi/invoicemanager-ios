//
//  InfoBarView.swift
//  ScanAndGo
//
//  Created by Can Zhan on 8/8/18.
//  Copyright © 2018 WOW. All rights reserved.
//

import UIKit

/// The Info Bar used to show either messages or toast on top of the screen.
@IBDesignable class InfoBarView: UIView {
    
    // MARK: - Vars
    
    /// Message (text with style) to be displayed
    var message: InfoBarMessage? {
        didSet {
            refreshMessage()
            refreshStyle()
        }
    }
    
    // MARK: - UI
    private let titleContainerView = UIView(frame: .zero)
    private let iconImageView = UIImageView(frame: .zero)
    private let titleLabel = UILabel(frame: .zero)
    private let messageContentLabel = UILabel(frame: .zero)
    private let toastContentLabel = UILabel(frame: .zero)
    
    private var stackViewTopConstraint: NSLayoutConstraint?
    private var stackViewBottomConstraint: NSLayoutConstraint?
    private var heightConstraint: NSLayoutConstraint?
    
    // MARK: - Computed Vars
    
    /// Attributed message title
    private var titleAttributedString: NSAttributedString? {
        guard let text = message?.title else { return nil }
        return StyleManager.attributedStringWith(title: text, weight: .medium, size: 14, textColor: .textGray)
    }
    
    /// Attributed message content
    private var contentAttributedString: NSAttributedString? {
        guard let text = message?.content else { return nil }
        return StyleManager.attributedStringWith(title: text, size: 12, textColor: .textGray)
    }
    
    /// The icon based on message type
    private var iconImage: UIImage? {
        guard let type = message?.type else { return nil }
        switch type {
        case .infoMessage, .infoToast: return R.image.icon_information()
        case .warningMessage, .warningToast: return R.image.icon_warning()
        case .errorMessage, .errorToast: return R.image.icon_warning()
        }
    }
    
    /// Top margin of stackView
    private var topConstraintConstant: CGFloat {
        return message != nil ? 16 : 0
    }
    
    /// Bottom margin of stackView
    private var bottomConstraintConstant: CGFloat {
        return message != nil ? -16 : 0
    }
    
    // MARK: - Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    /// Build the view
    private func commonInit() {
        // round corner & border
        layer.borderWidth = 1.0
        clipsToBounds = true
        
        // self
        translatesAutoresizingMaskIntoConstraints = false
        heightConstraint = heightAnchor.constraint(equalToConstant: 0.0)
        heightConstraint?.isActive = false
        
        // stack view
        let stackView = UIStackView(arrangedSubviews: [titleContainerView, messageContentLabel, toastContentLabel])
        stackView.spacing = 8.0
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        stackViewTopConstraint = stackView.topAnchor.constraint(equalTo: topAnchor, constant: topConstraintConstant)
        stackViewTopConstraint?.isActive = true
        stackViewBottomConstraint = stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: bottomConstraintConstant)
        stackViewBottomConstraint?.isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        stackView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        stackView.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
        
        // title container view
        titleContainerView.translatesAutoresizingMaskIntoConstraints = false
        titleContainerView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        titleContainerView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        
        // icon and title label
        titleLabel.numberOfLines = 2
        messageContentLabel.lineBreakMode = .byWordWrapping
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        titleContainerView.addSubview(iconImageView)
        titleContainerView.addSubview(titleLabel)
        
        iconImageView.centerYAnchor.constraint(equalTo: titleContainerView.centerYAnchor).isActive = true
        iconImageView.leadingAnchor.constraint(equalTo: titleContainerView.leadingAnchor).isActive = true
        iconImageView.trailingAnchor.constraint(equalTo: titleLabel.leadingAnchor, constant: -12).isActive = true
        iconImageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        iconImageView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        
        titleLabel.topAnchor.constraint(equalTo: titleContainerView.topAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: titleContainerView.bottomAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: titleContainerView.trailingAnchor).isActive = true
        
        // message content label
        messageContentLabel.numberOfLines = 0
        messageContentLabel.lineBreakMode = .byWordWrapping
        messageContentLabel.translatesAutoresizingMaskIntoConstraints = false
        messageContentLabel.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        messageContentLabel.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        
        // toast content label
        toastContentLabel.numberOfLines = 0
        toastContentLabel.translatesAutoresizingMaskIntoConstraints = false
        toastContentLabel.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        toastContentLabel.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        
        setNeedsLayout()
    }
    
    // MARK: - Functions

    /// Refresh/Reset current title with attributed strings
    private func refreshMessage() {
        // resize info bar based on updated message
        stackViewTopConstraint?.constant = topConstraintConstant
        stackViewBottomConstraint?.constant = bottomConstraintConstant
        
        guard let message = message else {
            // hide all if there is no message
            titleContainerView.isHidden = true
            messageContentLabel.isHidden = true
            toastContentLabel.isHidden = true
            heightConstraint?.isActive = true
            layoutIfNeeded()
            return
        }
        heightConstraint?.isActive = false
        
        iconImageView.image = iconImage
        switch message.type {
        case .infoMessage, .warningMessage, .errorMessage:
            titleLabel.attributedText = titleAttributedString
            titleContainerView.isHidden = false
            if let content = contentAttributedString {
                messageContentLabel.attributedText = content
                messageContentLabel.isHidden = false
            } else {
                messageContentLabel.isHidden = true
            }
            toastContentLabel.isHidden = true
            
        case .infoToast, .warningToast, .errorToast:
            titleContainerView.isHidden = true
            messageContentLabel.isHidden = true
            if let content = contentAttributedString {
                toastContentLabel.attributedText = content
                toastContentLabel.isHidden = false
            } else {
                toastContentLabel.isHidden = true
            }
            
        }
        
        layoutIfNeeded()
    }
    
    /// Refresh border and background color based on message type. Default to .errorMessage if message is not set.
    private func refreshStyle() {
        let type = message?.type ?? .errorMessage
        let backgroundColor: UIColor
        let borderColor: UIColor
        
        switch type {
        case .infoMessage, .infoToast:
            backgroundColor = UIColor.backgroundBlue
            borderColor = UIColor.borderBlue
        case .warningMessage, .warningToast:
            backgroundColor = UIColor.backgroundYellow
            borderColor = UIColor.borderYellow
        case .errorMessage, .errorToast:
            backgroundColor = UIColor.backgroundRed
            borderColor = UIColor.borderRed
        }
        
        self.backgroundColor = backgroundColor
        layer.borderColor = borderColor.cgColor
    }
    
}

