//
//  MultipleChoiceView.swift
//  InvoiceManager
//
//  Created by Can Zhan on 18/9/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

class MultipleChoiceView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        // add stack view
//        let
    }

}

protocol MultipleChoiceOption: CustomStringConvertible {
    
}

class MultipleChoiceViewModel {
    
    var selectedOption: MultipleChoiceOption?
    let options: [MultipleChoiceOption]
    
    init(options: [MultipleChoiceOption]) {
        self.options = options
    }
    
}
