//
//  IMRoundButton.swift
//  InvoiceManager
//
//  Created by Can Zhan on 17/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

@IBDesignable class IMRoundButton: IMButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = bounds.width / 2
        clipsToBounds = true
    }
    
}
