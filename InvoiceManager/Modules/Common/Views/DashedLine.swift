//
//  DashedLine.swift
//  ScanAndGo
//
//  Created by Can Zhan on 15/8/18.
//  Copyright © 2018 WOW. All rights reserved.
//

import UIKit

@IBDesignable class DashedLine: UIView {
    
    @IBInspectable var color: UIColor = .black { didSet { commonInit() } }
    @IBInspectable var dashWidth: Double = 6.0 { didSet { commonInit() } }
    @IBInspectable var spaceWidth: Double = 2.0 { didSet { commonInit() } }
    
    var lineDashPattern: [NSNumber] {
        return [NSNumber(value: dashWidth), NSNumber(value: spaceWidth)]
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = frame.height
        shapeLayer.lineDashPattern = lineDashPattern
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: frame.height / 2), CGPoint(x: frame.width, y: frame.height / 2)])
        shapeLayer.path = path
        
        layer.addSublayer(shapeLayer)
    }

}
