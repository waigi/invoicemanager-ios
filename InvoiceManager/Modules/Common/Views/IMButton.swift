//
//  IMButton.swift
//  ScanAndGo
//
//  Created by Can Zhan on 12/7/18.
//  Copyright © 2018 WOW. All rights reserved.
//

import UIKit

@IBDesignable class IMButton: UIButton {
    
    enum IMButtonType: Int {
        /// Green background with white text
        case primary
        /// Clear background with grey text
        case secondary
        /// Clear background with grey text
        case tertiary
    }
    
    /// Title string for the button
    @IBInspectable var titleString: String? {
        didSet {
            refreshTitle()
        }
    }
    
    /// Convenient var for Storyboard setting
    /// 0 : Primary IM button, default value
    /// 1 : Secondary IM button
    @IBInspectable var typeIntValue: Int = 0 {
        didSet {
            if let type = IMButtonType(rawValue: typeIntValue) {
                self.type = type
            }
        }
    }
    
    /// Type of IM button
    var type = IMButtonType.primary {
        didSet {
            refreshTitle()
            refreshBackground()
        }
    }
    
    // MARK: - Inits

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        // round corner
        layer.cornerRadius = 5.0
        clipsToBounds = true
        // attributed title
        refreshTitle()
        // background
        refreshBackground()
    }
    
    /// Refresh/Reset current title with attributed strings
    private func refreshTitle() {
        if let titleString = titleString ?? currentTitle {
            let activeTitleString: NSAttributedString
            let inactiveTitleString: NSAttributedString
            switch type {
            case .primary:
                activeTitleString = StyleManager.attributedStringWith(title: titleString, weight: .medium, size: 15, textColor: .primaryButtonText)
                inactiveTitleString = StyleManager.attributedStringWith(title: titleString, weight: .medium, size: 15, textColor: .cyan900 ?? .darkGray)
            case .secondary:
                activeTitleString = StyleManager.attributedStringWith(title: titleString, weight: .medium, size: 15, textColor: .secondaryButtonText)
                inactiveTitleString = StyleManager.attributedStringWith(title: titleString, weight: .medium, size: 15, textColor: .cyan900 ?? .darkGray)
            case .tertiary:
                activeTitleString = StyleManager.attributedStringWith(title: titleString, weight: .medium, size: 15, textColor: .tertiaryButtonText)
                inactiveTitleString = StyleManager.attributedStringWith(title: titleString, weight: .medium, size: 15, textColor: .cyan900 ?? .darkGray)
            }
            setAttributedTitle(activeTitleString, for: .normal)
            setAttributedTitle(inactiveTitleString, for: .disabled)
        }
    }
    
    private func refreshBackground() {
        switch type {
        case .primary:
//            let activeBackgroundImage = R.image.color_green()
//            let inactiveBackgroundImage = R.image.color_lightGray()
//            setBackgroundImage(activeBackgroundImage, for: .normal)
//            setBackgroundImage(inactiveBackgroundImage, for: .disabled)
            backgroundColor = .primaryButtonBackground
//            setBackgroundImage(nil, for: .normal)
//            setBackgroundImage(nil, for: .disabled)
        case .secondary:
            backgroundColor = .secondaryButtonBackground
//            setBackgroundImage(nil, for: .normal)
//            setBackgroundImage(nil, for: .disabled)
        case .tertiary:
            backgroundColor = .tertiaryButtonBackground
//            setBackgroundImage(nil, for: .normal)
//            setBackgroundImage(nil, for: .disabled)
        }
    }
    
}
