//
//  FileUtil.swift
//  InvoiceManager
//
//  Created by Can Zhan on 12/9/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics
import Zip

struct FileUtil {
    
    private struct Constants {
        static let imageExtension = "png"
    }
    
    /// A tuple consists two urls, former one for original sized image and latter one for the image's thumbnail
    typealias ImageWithThumbnailURL = (imageURL: URL, thumbnailURL: URL)
    
    /// App's Documents directory (backup applicable). e.g. "*/Application/FF5CB3BD-2C8C-42BB-86C0-7114120E1C79/Documents"
    static var homeDirectory: URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    }
    
    /// Create a "2018-09-12" like directory in App's "Document" directory if it doesn't exist and return the URL to the directory.
    static func createDirectory(for date: Date = Date()) -> URL? {
        guard let directoryUrl = invoicesDirectoryOrCreateItIfNeeded?.appendingPathComponent(date.formattedYearMonthDateString, isDirectory: true) else { return nil }
        guard !FileManager.default.fileExists(atPath: directoryUrl.path) else {
            return directoryUrl
        }
        do {
            try FileManager.default.createDirectory(at: directoryUrl, withIntermediateDirectories: true)
            return directoryUrl
        } catch {
            return nil
        }
    }
    
    /// Move source file at url to the directory of specified date. For instance, user picked a photo abc.png, it will be moved into
    /// 'app/home/dir/2018-09-12/1-hi-res.png'
    static func moveFile(at url: URL, to directoryOfDate: Date = Date()) -> URL? {
        // check if the desctination directory exist, if not, create it
        guard let destDirUrl = FileUtil.createDirectory(for: directoryOfDate) else { return nil }
        do {
            // check the number of files
            let fileNumber = try FileManager.default.contentsOfDirectory(atPath: destDirUrl.path).count
            // generate new file name by incrementing sequence number
            let destFileUrl = destDirUrl.appendingPathComponent("\(fileNumber + 1)-hi-res").appendingPathExtension(url.pathExtension)
            // move file
            try FileManager.default.moveItem(at: url, to: destFileUrl)
            return destFileUrl
        } catch {
            return nil
        }
    }
    
    /// Save an image with its thumbnail into the given date's directory, and return two saved URLs if saved successfully.
    static func saveImage(_ image: UIImage, with thumbnail: UIImage, for date: Date) -> ImageWithThumbnailURL? {
        guard let imageURL = FileUtil.nextImageFileURL(for: date),
            let data = image.jpegData(compressionQuality: 1.0), //image.pngData(), use jpg as it's less than half of the size and only jpg file works with Google ML Kit as of now
            let thumbnailURL = FileUtil.nextImageFileURL(for: date, isThumbnail: true),
            let thumbnailData = thumbnail.jpegData(compressionQuality: 1.0) //thumbnail.pngData() use jpg as it's less than half of the size and only jpg file works with Google ML Kit as of now
            else { return nil }
        do {
            try data.write(to: imageURL)
            try thumbnailData.write(to: thumbnailURL)
        } catch {
            print(error)
            return nil
        }
        return (imageURL, thumbnailURL)
    }
    
    /// Save an image into the date's directory
    static func saveImage(_ image: UIImage, for date: Date = Date()) -> URL? {
        guard let url = FileUtil.nextImageFileURL(for: date), let data = image.pngData() else { return nil }
        do {
            try data.write(to: url)
        } catch {
            return nil
        }
        return url
    }
    
    /// Calculate the next available file name for the specified date.
    private static func nextImageFileURL(for date: Date = Date(), isThumbnail: Bool = false) -> URL? {
        // check if the desctination directory exist, if not, create it
        guard let destDirUrl = FileUtil.createDirectory(for: date) else { return nil }
        // check the number of files
//            let fileNumber = try FileManager.default.contentsOfDirectory(atPath: destDirUrl.path).count / 2
        // use timestamp to replace number of files to avoid duplicate filenames
        let fileNumber = date.timeIntervalSince1970
        // generate new file name by incrementing sequence number
        let destFileUrl = destDirUrl.appendingPathComponent("\(fileNumber)-\(isThumbnail ? "low" : "hi")-res").appendingPathExtension(Constants.imageExtension)
        return destFileUrl
    }
    
    /// Load an image with given file path. Return the loaded image or nil if failed to load.
    /// - Parameter filePath: file relative path to home directory
    static func loadImage(from filePath: String) -> UIImage? {
        guard let url = FileUtil.homeDirectory?.appendingPathComponent(filePath) else {
            print("Failed to load image from \(filePath)")
            return nil
        }
        do {
            let data = try Data(contentsOf: url)
            let image = UIImage(data: data)
            print("Successfully load image from \(url)")
            return image
        } catch {
            print("Failed to load image from \(url) with error:\n\(error)")
        }
        return nil
    }
    
    /// Delete an image with given relative file path.
    /// - Parameter filePath: file relative path to home directory
    static func deleteFile(from filePath: String) -> Bool {
        guard let url = FileUtil.homeDirectory?.appendingPathComponent(filePath) else {
            print("Failed to find file from \(filePath)")
            return false
        }
        do {
            try FileManager.default.removeItem(at: url)
            print("Successfully delete file from \(url)")
            return true
        } catch {
            print("Failed to delete file from \(url) with error:\n\(error)")
            return false
        }
    }
    
    /// Delete a file from url
    static func deleteFile(from url: URL?) -> Bool {
        guard let fileURL = url else {
            print("Failed to find file from \(url)")
            return false
        }
        do {
            try FileManager.default.removeItem(at: fileURL)
            print("Successfully delete file from \(fileURL)")
            return true
        } catch {
            print("Failed to delete file from \(fileURL) with error:\n\(error)")
            return false
        }
    }
    
    // MARK: - Export invoices
    
    /// All invoice photos are resided here. If it doesn't exist, create it first and then return the URL
    static var invoicesDirectoryOrCreateItIfNeeded: URL? {
        guard let directoryURL = FileUtil.homeDirectory?.appendingPathComponent("invoices", isDirectory: true) else { return nil }
        if !FileManager.default.fileExists(atPath: directoryURL.path) {
            do {
                try FileManager.default.createDirectory(at: directoryURL, withIntermediateDirectories: true)
            } catch {
                AnalyticsUtil.trackError(error)
                return nil
            }
        }
        return directoryURL
    }
    
    /// The file URL to where invoices should be exported
    static var exportedCSVFileURL: URL? {
        if let exportedFileURL = invoicesDirectoryOrCreateItIfNeeded?.appendingPathComponent("ExportedInvoices.csv") {
            // delete the old one if it exists before exporting
            if FileManager.default.fileExists(atPath: exportedFileURL.path) {
                do {
                    try FileManager.default.removeItem(at: exportedFileURL)
                } catch {
                    AnalyticsUtil.trackError(error)
                    return nil
                }
            }
            return exportedFileURL
        }
        return nil
    }
    
    /// The file URL to where invoices should be exported
    static var exportedHTMLFileURL: URL? {
        if let exportedFileURL = invoicesDirectoryOrCreateItIfNeeded?.appendingPathComponent("ExportedInvoices.html") {
            // delete the old one if it exists before exporting
            if FileManager.default.fileExists(atPath: exportedFileURL.path) {
                do {
                    try FileManager.default.removeItem(at: exportedFileURL)
                } catch {
                    AnalyticsUtil.trackError(error)
                    return nil
                }
            }
            return exportedFileURL
        }
        return nil
    }
    
    /// Export Invoices into excel
    static func exportInvoicesCSV() -> URL? {
        var string = "Financial Year,Business Name,ABN,Date,Category,Total Amount,GST,Note,Extracted\n"
        PersistanceUtil.loadInvoices().forEach {
            string += "\($0.invoiceDate?.financialYearString ?? ""),\"\($0.businessName)\",\($0.abn ?? ""),\($0.invoiceDate?.formattedYearMonthDateString ?? ""),\($0.categoryString),\($0.totalAmountIncludeGst.rawDollarString),\($0.gst.rawDollarString),\"\($0.note ?? "")\",\"\($0.fullContent ?? "")\"\n"
        }
        guard let exportedFileURL = exportedCSVFileURL else { return nil}
        do {
            try string.write(to: exportedFileURL, atomically: true, encoding: .utf8)
        } catch {
            AnalyticsUtil.trackError(error)
            return nil
        }
        return exportedFileURL
    }
    
    /// Export Invoices into HTML
    static func exportInvoicesHTML() -> URL? {
        var string = """
            <html>
            <body>
                <table>
                  <tr>
                    <th>Financial Year</th>
                    <th>Business Name</th>
                    <th>ABN</th>
                    <th>Date</th>
                    <th>Category</th>
                    <th>Total Amount</th>
                    <th>GST</th>
                    <th>Note</th>
                    <th>Invoice Image</th>
                  </tr>
            """
        PersistanceUtil.loadInvoices().forEach {
            string += """
            <tr>
                <td>\($0.invoiceDate?.financialYearString ?? "")</td>
                <td>\($0.businessName)</td>
                <td>\($0.abn ?? "")</td>
                <td>\($0.invoiceDate?.formattedYearMonthDateString ?? "")</td>
                <td>\($0.categoryString)</td>
                <td>\($0.totalAmountIncludeGst.rawDollarString)</td>
                <td>\($0.gst.rawDollarString)</td>
                <td>\($0.note ?? "")</td>
                <td><a href="\($0.localImageHTMLRelativePath ?? "")"><img src="\($0.localThumbnailHTMLRelativePath ?? "")" width="60" height="80"></a></td>
            </tr>
            """
        }
        string += """
            </table>
        </body>
        </html>
        """
        guard let exportedFileURL = exportedHTMLFileURL else { return nil }
        do {
            try string.write(to: exportedFileURL, atomically: true, encoding: .utf8)
        } catch {
            AnalyticsUtil.trackError(error)
            return nil
        }
        return exportedFileURL
    }
    
    static func zipInvoicesDirectory(progress: (((Double) -> ())?) = nil, success: ((URL) -> Void), failure: (() -> Void)) {
        guard let invoiceDirectory = invoicesDirectoryOrCreateItIfNeeded,
            let outputFile = URL(string: "Invoices", relativeTo: homeDirectory)?.path else {
                failure()
                return
        }
        do {
            let zipFilePath = try Zip.quickZipFiles([invoiceDirectory],
                                                    fileName: "Invoices",
                                                    progress: progress)
            success(zipFilePath)
        } catch {
            AnalyticsUtil.trackError(error)
            failure()
        }
    }
    
    static func exportAndZipInvoices(progress: (((Double) -> ())?) = nil, success: ((URL) -> Void), failure: (() -> Void)) {
        _ = exportInvoicesCSV() // for testing
        guard let _ = exportInvoicesHTML() else {
            failure()
            return
        }
        zipInvoicesDirectory(progress: progress, success: success, failure: failure)
    }
}
