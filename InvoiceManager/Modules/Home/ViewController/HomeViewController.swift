//
//  HomeViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 17/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

class HomeViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = 1
    }

}
