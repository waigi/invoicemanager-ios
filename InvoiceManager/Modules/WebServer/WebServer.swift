//
//  WebServer.swift
//  InvoiceManager
//
//  Created by Can Zhan on 2/1/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import Foundation
import GCDWebServer

struct WebServer {
    
    static var filePath: String?
    
    /// Web server instance
    private static let webServer: GCDWebServer = {
        let webServer = GCDWebServer()
        webServer.addDefaultHandler(
            forMethod: "GET",
            request: GCDWebServerRequest.self,
            processBlock: { (request) -> GCDWebServerResponse? in
                if let filePath = WebServer.filePath {
                    return GCDWebServerFileResponse(file: filePath, isAttachment: true)
                } else {
                    return GCDWebServerDataResponse(html: "<html><body>couldn't find file to download, please retry export</body></html>")
                }
        })
        return webServer
    }()
    
    /// Start the web server and returns the URL to access it
    static func start() -> URL? {
        webServer.start(withPort: 8080, bonjourName: "Invoices Download Server")
        return webServer.serverURL
    }
    
    /// Stop the web server
    static func stop() {
        webServer.stop()
    }
    
}
