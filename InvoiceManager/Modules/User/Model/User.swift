//
//  User.swift
//  InvoiceManager
//
//  Created by Can Zhan on 18/12/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class User: Object {
    dynamic var username: String = ""
    /// Current selected business
    dynamic var business: Business?
    /// All businesses
    dynamic let businesses = List<Business>()
    
    // MARK: - computed vars
    
    /// If there is at least one business belongs to the user. If not, user must add business before starting.
    var hasBusiness: Bool { return !businesses.isEmpty }
}

@objcMembers
class Business: Object {
    dynamic var id: String = ""
    dynamic var name: String = ""
    dynamic var createDate: Date = Date()
    
    convenience init(name: String) {
        self.init()
        self.name = name
    }
}

// MARK: - Convenience function
extension User {
    static var selectedBusinessName: String? {
        return PersistanceUtil.loadUser().business?.name
    }
}
