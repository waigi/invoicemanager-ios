//
//  SettingViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 2/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

protocol SettingDelegate: class {
    func invoiceDefaultsDidUpdate(invoiceDate: Date, invoiceCategory: InvoiceCategory, totalAmount: Double, gstAmount: Double, abn: String?, note: String?)
}

class SettingViewController: UIViewController {
    
    // MARK: - IBOutlets

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var cashPaymentTick: TickBoxView!
    @IBOutlet weak var cashPaymentLabel: UILabel!
    @IBOutlet weak var eftPaymentTick: TickBoxView!
    @IBOutlet weak var eftPaymentLabel: UILabel!
    @IBOutlet weak var totalAmountTextField: UITextField!
    @IBOutlet weak var gstAmountTextField: UITextField!
    @IBOutlet weak var abnTextField: UITextField!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: - Vars
    
    weak var delegate: SettingDelegate?
    let viewModel = SettingViewModel()
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cashPaymentTick.delegate = self
        eftPaymentTick.delegate = self
        cashPaymentLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingViewController.tappedCashPaymentLabel)))
        eftPaymentLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingViewController.tappedEFTPaymentLabel)))
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(SettingViewController.tappedBackgroundView)))
        totalAmountTextField.addTarget(self, action: #selector(SettingViewController.autoFillGSTAmount), for: .editingChanged)
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Register keyboard notification
        NotificationCenter.default.addObserver(self, selector: #selector(SettingViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Deregister keyboard observer
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func setupUI() {
        contentView.applyShadow(shadowColor: .darkGray)
        datePicker.date = viewModel.invoiceDate
        tickCategory(viewModel.invoiceCategory)
        totalAmountTextField.text = viewModel.totalAmount != 0.0 ? viewModel.totalAmount.string : nil
        gstAmountTextField.text = viewModel.gstAmount != 0.0 ? viewModel.gstAmount.string : nil
        totalAmountTextField.applyKeyboardDoneButton()
        gstAmountTextField.applyKeyboardDoneButton()
        abnTextField.text = viewModel.abn
        noteTextView.text = viewModel.note
        abnTextField.applyKeyboardDoneButton()
        noteTextView.applyKeyboardDoneButton()
        noteTextView.applyBorder(borderColor: .cyan50)
        noteTextView.contentInset = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    }
    
    // MARK: - IBActions
    
    @IBAction func tappedCancelButton() {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tappedSaveButton() {
        viewModel.invoiceDate = datePicker.date
        if let totalAmountString = totalAmountTextField.text,
            let totalAmount = Double(totalAmountString) {
            viewModel.totalAmount = totalAmount
        }
        if let gstAmountString = gstAmountTextField.text,
            let gstAmount = Double(gstAmountString) {
            viewModel.gstAmount = gstAmount
        }
        if let abn = abnTextField.text, abn.isABN {
            viewModel.abn = abn
        }
        if let note = noteTextView.text {
            viewModel.note = note
        }
        delegate?.invoiceDefaultsDidUpdate(invoiceDate: viewModel.invoiceDate, invoiceCategory: viewModel.invoiceCategory, totalAmount: viewModel.totalAmount, gstAmount: viewModel.gstAmount, abn: viewModel.abn, note: viewModel.note)
        dismiss(animated: true, completion: nil)
    }

    @IBAction func pickedDate(_ sender: UIDatePicker) {
    }
    
    @IBAction func tappedTodayButton() {
        datePicker.date = Date()
    }
    
    // MARK: - Functions
    
    /// Select a invoice category
    private func tickCategory(_ category: InvoiceCategory) {
        switch category {
        case .cashPayment:
            cashPaymentTick.tick()
            eftPaymentTick.untick()
        case .eftPayment:
            cashPaymentTick.untick()
            eftPaymentTick.tick()
        }
        viewModel.invoiceCategory = category
    }
    
    /// Dismiss keyboard
    @objc private func tappedBackgroundView() {
        view.endEditing(true)
    }
    
    @objc private func tappedCashPaymentLabel() {
        tickCategory(.cashPayment)
    }
    
    @objc private func tappedEFTPaymentLabel() {
        tickCategory(.eftPayment)
    }
    
    // MARK: - Keyboard notification handlers
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        let bottomCoveredView: UITextView = noteTextView
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let bottomCoveredViewSuperview = bottomCoveredView.superview {
            // move screen up if keyboard covers the GST textfield
            let convertedFrame = view.convert(bottomCoveredView.frame, from: bottomCoveredViewSuperview)
            let coveredHeight = convertedFrame.maxY - (keyboardSize.minY - 8)
            if coveredHeight > 0 {
                scrollView.contentOffset.y += coveredHeight
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        scrollView.contentOffset.y = 0
    }
}

// MARK: - TickBoxViewDelegate : update with selected category
extension SettingViewController: TickBoxViewDelegate {
    func tickBoxViewDidChange(isTicked: Bool, tickBoxView: TickBoxView) {
        guard isTicked else { return }
        switch tickBoxView {
        case cashPaymentTick:
            tickCategory(.cashPayment)
            viewModel.invoiceCategory = .cashPayment
        case eftPaymentTick:
            tickCategory(.eftPayment)
            viewModel.invoiceCategory = .eftPayment
        default: break
        }
    }
}

// MARK: - UITextFieldDelegate : Automatically fill in GST amount while entering total amount
extension SettingViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == totalAmountTextField {
            autoFillGSTAmount()
        }
        if let totalAmountString = totalAmountTextField.text, let totalAmount = Double(totalAmountString) {
            totalAmountTextField.text = totalAmount.string
        } else {
            totalAmountTextField.text = "0.00"
        }
        if let gstAmountString = gstAmountTextField.text, let gstAmount = Double(gstAmountString) {
            gstAmountTextField.text = gstAmount.string
        } else {
            gstAmountTextField.text = "0.00"
        }
    }
    
    /// Format total amount and automatically fill in GST amount
    @objc private func autoFillGSTAmount() {
        if let totalAmountString = totalAmountTextField.text, let totalAmount = Double(totalAmountString) {
            gstAmountTextField.text = (totalAmount / 11).string
        }
    }
}

class SettingViewModel {
    
    var invoiceDate: Date = UserDefaults.defaultInvoiceDate
    
    var invoiceCategory: InvoiceCategory = UserDefaults.defaultCategory
    
    /// Grand Total amount include GST
    var totalAmount: Double = 0.0
    /// GST amount
    var gstAmount: Double = 0.0
    /// ABN String
    var abn: String?
    /// Invoice Note
    var note: String?
}
