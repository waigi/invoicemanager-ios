//
//  Invoice.swift
//  InvoiceManager
//
//  Created by Can Zhan on 26/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation
import RealmSwift

enum InvoiceCategory: String, CustomStringConvertible, Codable {
    case cashPayment = "Cash Payment"
    case eftPayment = "EFT Payment"
    
    var description: String {
        switch self {
        case .cashPayment : return "Cash Payment"
        case .eftPayment : return "EFT Payment"
        }
    }
}

@objcMembers
class Invoice: Object {
    dynamic var id: String = ""
    dynamic var localThumbnailURLAbsoluteString: String?
    dynamic var localImageURLAbsoluteString: String?
    /// local file path related to Home Directory
    dynamic var localThumbnailRelativePath: String?
    /// local file path related to Home Directory
    dynamic var localImageRelativePath: String?
    dynamic var remoteThumbnailURLString: String?
    dynamic var remoteImageURLString: String?
    dynamic var isUploaded: Bool = false
    dynamic var invoiceDate: Date?
    dynamic var createDate: Date = Date()
    dynamic var sequenceInTheDay: Int = 0
    dynamic var categoryString: String = InvoiceCategory.cashPayment.rawValue
    /// below fields are optional and will be extracted from images by Vision
    dynamic var company: String?
    dynamic var abn: String?
    dynamic var phone: String?
    dynamic var address: String?
    dynamic var gst: Double = 0.0 // set to be 0.0 as Realm doesn't support Optional Double
    dynamic var totalAmountIncludeGst: Double = 0.0
    /// full detected invoice content, might be used for searching
    dynamic var fullContent: String?
    dynamic var businessName: String = ""
    /// user input note of the invoice
    dynamic var note: String?
    /// Possiblely used to save payment card in the future
    dynamic var paymentCardNumber: String?
    
    // non-persistent properties
    var category: InvoiceCategory {
        get {
            return InvoiceCategory(rawValue: categoryString) ?? .cashPayment
        }
        set {
            categoryString = newValue.rawValue
        }
    }
    
    override static func ignoredProperties() -> [String] {
        return ["category"]
    }
    
    /// HTML path pointing to the images, used for exporting HTML
    var localThumbnailHTMLRelativePath: String? {
        guard let path = localThumbnailRelativePath else { return nil }
        return path.replacingOccurrences(of: "/invoices", with: ".")
    }
    
    /// HTML path pointing to the images, used for exporting HTML
    var localImageHTMLRelativePath: String? {
        guard let path = localImageRelativePath else { return nil }
        return path.replacingOccurrences(of: "/invoices", with: ".")
    }
}

@objcMembers
class DailyInvoice: Object {
    dynamic var id: String = ""
    dynamic var date: Date = Date()
    let invoices = List<Invoice>()
    let uploadedInvoices = List<Invoice>()
    let pendingUploadedInvoices = List<Invoice>()
    
    // MARK: - Computed Vars : not persisted
    var nextSequenceInTheDay: Int {
        return (invoices.sorted{ $0.sequenceInTheDay > $1.sequenceInTheDay }.first?.sequenceInTheDay ?? 0) + 1
    }
    
    /// Sort by createDate
//    var sortedInvoices: [Invoice] {
//        return invoices.map({ $0 }).sorted { $0.createDate > $1.createDate }
//    }
    
    /// If there is matching invoice with filter criteria (businessName at the moment)
    func hasInvoices(businessName: String?) -> Bool {
        return !filteredInvoices(businessName: businessName).isEmpty
    }
    
    /// Filtered by business name and then sort by createDate
    func filteredInvoices(businessName: String?) -> [Invoice] {
        guard let businessName = businessName else { return [] }
        return invoices.map{ $0 }.filter{ $0.businessName == businessName }.sorted{ $0.createDate > $1.createDate }
    }
}

@objcMembers
class MonthlyInvoice: Object {
    dynamic var id: String = ""
    dynamic var date: Date = Date()
    let dailyInvoices = List<DailyInvoice>()
    
    // MARK: - Computed Vars : not persisted
    
    /// Sort by createDate
//    var sortedDailyInvoices: [DailyInvoice] {
//        return dailyInvoices.map({ $0 }).sorted { $0.date > $1.date }
//    }
    
    /// If there is matching invoice with filter criteria (businessName at the moment)
    func hasDailyInvoices(businessName: String?) -> Bool {
        return !filteredDailyInvoices(businessName: businessName).isEmpty
    }
    
    /// Filtered by business name and then sort by createDate
    func filteredDailyInvoices(businessName: String?) -> [DailyInvoice] {
        guard let businessName = businessName else { return [] }
        return dailyInvoices.map{ $0 }.filter{ $0.hasInvoices(businessName: businessName) }.sorted{ $0.date > $1.date }
    }
}
