//
//  CameraViewModel.swift
//  InvoiceManager
//
//  Created by Can Zhan on 2/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit
import UserNotifications

class CameraViewModel {
    
    var invoiceDate: Date {
        get {
            return UserDefaults.defaultInvoiceDate
        }
        set {
            UserDefaults.defaultInvoiceDate = newValue
        }
    }
    
    var invoiceCategory: InvoiceCategory {
        get {
            return UserDefaults.defaultCategory
        }
        set {
            UserDefaults.defaultCategory = newValue
        }
    }
    
    /// Grand Total amount include GST
    var totalAmount: Double = 0.0
    /// GST amount
    var gstAmount: Double = 0.0
    /// Invoice image thumbnail
    var thumbnail: UIImage?
    /// Invoice image
    var image: UIImage? {
        didSet {
            thumbnail = image?.renderResizedImage(newWidth: thumbnailSize.width)
        }
    }
    var abn: String?
    var note: String?
    
    /// Thumbnail of invoice images
    let thumbnailSize = CGSize(width: 120, height: 160)
    
    // MARK: - Functions
    
    /// Save captured or picked invoice photo with it's thumbnail into the correct folder and add invoice into DB.
    func saveInvoiceIfPossible() {
        if let image = image, let thumbnail = thumbnail {
            persistCapturedImage(image, with: thumbnail)
        }
    }
    
    private func persistCapturedImage(_ image: UIImage, with thumbnail: UIImage) {
        /// save the invoice info into Database
        func updateInvoiceDB(urls: FileUtil.ImageWithThumbnailURL) -> Bool {
            return PersistanceUtil.saveInvoice(
                of: invoiceDate,
                thumbnailURL: urls.thumbnailURL,
                imageURL: urls.imageURL,
                category: invoiceCategory,
                totalAmount: totalAmount,
                gstAmount: gstAmount,
                abn: abn,
                note: note)
        }
        
        if let urls = FileUtil.saveImage(image, with: thumbnail, for: Date()) {
            DispatchQueue.main.async {
                if updateInvoiceDB(urls: urls) {
                    print("Successfully saved image at \(urls).")
                    CameraViewModel.postNotification(title: "Successfully Saved", body: "The invoice was saved successfully. View it in 'Invoices' or keep adding more.")
                } else {
                    print("Failed to save image at \(urls).")
                    CameraViewModel.postNotification(title: "Failed to Save", body: "The invoice was not saved. Please check if there is enough storage or try again.")
                }
            }
        } else {
            print("No image found. Failed to save image.")
            CameraViewModel.postNotification(title: "Failed to Save", body: "The invoice was not saved. Please check if there is enough storage or try again.")
        }
    }
}

extension CameraViewModel {
    /// Post a local notification
    private static func postNotification(title: String = "", body: String = "") {
        // content
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        // trigger
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        // request
        let request = UNNotificationRequest(identifier: "invoiceNotification", content: content, trigger: trigger)
        // post
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}
