//
//  CameraViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 17/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit
import AVFoundation

class CameraViewController: UIViewController {
    
    // MARK: - Enums
    enum State {
        /// Showing camera overlay
        case camera
        /// Preview the shot image
        case image
    }

    // MARK: - IBOutlets
    @IBOutlet weak var previewView: CameraPreviewView!
    @IBOutlet weak var capturedImageView: UIImageView!
    @IBOutlet weak var buttonsContainerView: UIView!
    @IBOutlet weak var shutterButton: IMRoundButton!
    @IBOutlet weak var cancelButton: IMRoundButton!
    @IBOutlet weak var settingButton: IMRoundButton!
    @IBOutlet weak var confirmButton: IMRoundButton!
    @IBOutlet weak var confirmButtonCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButtonCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var propertiesView: UIView!
    @IBOutlet weak var invoiceDateLabel: UILabel!
    @IBOutlet weak var invoiceCategoryTagView: TagView!
    
    // MARK: - Vars
    
    private let viewModel = CameraViewModel()
    /// Concurrent queue to execute camera and image related functions
    private let cameraQueue = DispatchQueue(label: "cameraQueue")
    
    /// capturing photo or showing photo
    private var state: State = .camera {
        didSet {
            let backView: UIView = state == .camera ? capturedImageView : previewView
            view.sendSubviewToBack(backView)
            propertiesView.isHidden = state == .camera
        }
    }
    
    /// capture session
    private let captureSession = AVCaptureSession()
    /// photo output
    private let photoOutput = AVCapturePhotoOutput()
    
    // MARK: - Computed Vars
    
    /// photo setting, may not be reused,thus generate every time
    private var photoSettings: AVCapturePhotoSettings {
        // init
        let settings: AVCapturePhotoSettings
        if photoOutput.availablePhotoCodecTypes.contains(.hevc) {
            settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.hevc])
        } else {
            settings = AVCapturePhotoSettings()
        }
//        settings.isHighResolutionPhotoEnabled = true
        // preview
//        if !settings.availablePreviewPhotoPixelFormatTypes.isEmpty,
//            let formatType = settings.availablePreviewPhotoPixelFormatTypes.first {
//            settings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey : formatType,
//                                           kCVPixelBufferWidthKey : viewModel.thumbnailSize.width,
//                                           kCVPixelBufferHeightKey : viewModel.thumbnailSize.height] as [String: Any]
//        }
        // thumbnail
//        settings.embeddedThumbnailPhotoFormat = [
//            AVVideoCodecKey: AVVideoCodecType.jpeg,
//            AVVideoWidthKey: viewModel.thumbnailSize.width,
//            AVVideoHeightKey: viewModel.thumbnailSize.height,
//        ]
        return settings
    }
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updatePropertiesView()
        requestAuthorizationIfNeeded()
        NotificationCenter.default.addObserver(self, selector: #selector(CameraViewController.requestAuthorizationIfNeeded), name: UIApplication.didBecomeActiveNotification, object: nil)
        // Clear previous values
        viewModel.totalAmount = 0.0
        viewModel.gstAmount = 0.0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopCaptureSessionIfNeeded()
        NotificationCenter.default.removeObserver(self)
        resetUI()
    }
    
    // MARK: - UI
    
    private func setupUI() {
        // previewView
        previewView.videoPreviewLayer.session = captureSession
        // confirmView
        updatePropertiesView()
    }
    
    /// Reset UI to the default values
    private func resetUI() {
        // reset invoice date to be today whenever back to this screen
        UserDefaults.defaultInvoiceDate = Date()
        // cancel uncompleted photo if there is any
        if state == .image {
            tappedCancelButton()
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func tappedShutterButton() {
        takePhoto()
        toggleButtons()
    }
    
    @IBAction func tappedCancelButton() {
        setupIfNeededAndStartCaptureSession()
        toggleButtons()
        state = .camera
    }
    
    @IBAction func tappedSettingButton() {
    }
    
    @IBAction func tappedConfirmButton() {
        cameraQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.viewModel.saveInvoiceIfPossible()
        }
        setupIfNeededAndStartCaptureSession()
        toggleButtons()
        state = .camera
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let settingViewController = segue.destination as? SettingViewController {
            settingViewController.delegate = self
            settingViewController.viewModel.totalAmount = viewModel.totalAmount
            settingViewController.viewModel.gstAmount = viewModel.gstAmount
            settingViewController.viewModel.abn = viewModel.abn
            settingViewController.viewModel.note = viewModel.note
        }
    }
    
    // MARK: - Buttons
    
    /// Show either shutter button to capture invoice or the other buttons to cancel/set/confirm
    private func toggleButtons() {
        shutterButton.isHidden = state == .camera ? true : false
        cancelButton.isHidden = state == .camera ? false : true
        settingButton.isHidden = state == .camera ? false : true
        confirmButton.isHidden = state == .camera ? false : true
        buttonsContainerView.bringSubviewToFront(state == .camera ? settingButton : shutterButton)
        let offset = state == .camera ? (buttonsContainerView.frame.width / 2 + shutterButton.frame.width / 2) / 2 : 0
        confirmButtonCenterXConstraint.constant = offset
        if state == .camera {
            cancelButtonCenterXConstraint.constant = -offset
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.buttonsContainerView.layoutIfNeeded()
            }
        } else {
            cancelButtonCenterXConstraint.constant = offset
        }
        state = state == .camera ? .image : .camera
    }
    
    // MARK: - Camera
    
    /// Check the permission of accessing camera, If it's authorized, launch the capture session, otherwise, wait until user gives permission
    @objc private func requestAuthorizationIfNeeded() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            setupIfNeededAndStartCaptureSession()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { [weak self] (granted) in
                guard let strongSelf = self else { return }
                if granted {
                    strongSelf.setupIfNeededAndStartCaptureSession()
                } else {
                    strongSelf.showPermissionRequestAlert()
                }
            }
        case .denied, .restricted:
            showPermissionRequestAlert()
        }
    }
    
    /// Show a pop up to ask for camera access privilege
    private func showPermissionRequestAlert() {
        let alert = UIAlertController(title: "Permission Required",
                                      message: "Please enable camera usage in APP Settings to be able to record invoices.",
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (action) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    /// Configure the capture session if it has not been done before and start the capture session if it's not running
    private func setupIfNeededAndStartCaptureSession() {
        cameraQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            // configue if needed
            if strongSelf.captureSession.inputs.isEmpty {
                print("\n\(#function) : do configuration\n")
                strongSelf.captureSession.beginConfiguration()
                // input
                guard let videoDevice = AVCaptureDevice.default(.builtInDualCamera,
                                                                for: .video,
                                                                position: .unspecified) ??
                                        AVCaptureDevice.default(.builtInWideAngleCamera,
                                                                for: .video,
                                                                position: .unspecified),
                    let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice),
                    strongSelf.captureSession.canAddInput(videoDeviceInput)
                    else { return }
                strongSelf.captureSession.addInput(videoDeviceInput)
                
                // output
                guard strongSelf.captureSession.canAddOutput(strongSelf.photoOutput) else { return }
                strongSelf.captureSession.sessionPreset = .photo
                strongSelf.captureSession.addOutput(strongSelf.photoOutput)
                
                strongSelf.captureSession.commitConfiguration()
            }
            
            if !strongSelf.captureSession.isRunning {
                print("\n\(#function) : start running\n")
                strongSelf.captureSession.startRunning()
            }
        }
    }
    
    /// Stop capture session if it's running
    private func stopCaptureSessionIfNeeded() {
        if captureSession.isRunning {
            print("\n\(#function) : stop running\n")
            captureSession.stopRunning()
        }
    }
    
    /// Capture a photo from photo output
    private func takePhoto() {
        cameraQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.photoOutput.capturePhoto(with: strongSelf.photoSettings, delegate: strongSelf)
        }
    }
    
    // MARK: - Functions
    
    private func updatePropertiesView() {
        invoiceDateLabel.text = viewModel.invoiceDate.formattedDateMonthYearString
        invoiceCategoryTagView.invoiceCategory = viewModel.invoiceCategory
    }

}

// MARK: - AVCapturePhotoCaptureDelegate : Capture photo
extension CameraViewController: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        cameraQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            if let data = photo.fileDataRepresentation(),
                let image = UIImage(data: data) {
//                let thumbnail = image.renderResizedImage(newWidth: strongSelf.viewModel.thumbnailSize.width)
                strongSelf.viewModel.image = image
                TextRecognitionUtil.recognizeTextFrom(image) { (text) in
                    strongSelf.viewModel.abn = text.abn
                    let totalWithGST = text.totalWithGST
                    strongSelf.viewModel.totalAmount = totalWithGST.total
                    strongSelf.viewModel.gstAmount = totalWithGST.gst
                }
//                strongSelf.viewModel.thumbnail = thumbnail
                
//                guard let previewPixelBuffer = photo.previewPixelBuffer else { return }
//                let ciImage = CIImage(cvPixelBuffer: previewPixelBuffer)
//                let uiImage = UIImage(ciImage: ciImage)
//                strongSelf.viewModel.thumbnail = uiImage

//                DispatchQueue.main.async {
//                    strongSelf.capturedImageView.image = uiImage
//                    strongSelf.capturedImageView.image = image
//                    strongSelf.state = .image
//                }
                // only stop session after photo is taken
                strongSelf.stopCaptureSessionIfNeeded()
            }
        }
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        // Flash the screen to signal that AVCam took a photo.
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.previewView.videoPreviewLayer.opacity = 0
            UIView.animate(withDuration: 0.25) {
                strongSelf.previewView.videoPreviewLayer.opacity = 1
            }
        }
    }
    
//    private func thumbnail(for photo: AVCapturePhoto) -> UIImage? {
//        guard let cgImage = photo.previewCGImageRepresentation()?.takeRetainedValue() else { return nil }
//        return UIImage(cgImage: cgImage)
//    }
}

// MARK: - SettingDelegate : Handle invoice defaults
extension CameraViewController: SettingDelegate {
    func invoiceDefaultsDidUpdate(invoiceDate: Date, invoiceCategory: InvoiceCategory, totalAmount: Double, gstAmount: Double, abn: String?, note: String?) {
        UserDefaults.defaultInvoiceDate = invoiceDate
        UserDefaults.defaultCategory = invoiceCategory
        viewModel.totalAmount = totalAmount
        viewModel.gstAmount = gstAmount
        viewModel.abn = abn
        viewModel.note = note
        updatePropertiesView()
    }
}
