//
//  PersistanceUtil.swift
//  InvoiceManager
//
//  Created by Can Zhan on 27/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation
import RealmSwift

struct PersistanceUtil {
    
    // MARK: - Invoice
    
    /// Load all monthlyInvoices
    static func loadAllMonthlyInvoices() -> [MonthlyInvoice] {
        guard let realm = try? Realm(), let businessName = loadUser().business?.name else { return [] }
        let predicate = NSPredicate(format: "ANY dailyInvoices.invoices.businessName == %@", businessName)
        let results = realm.objects(MonthlyInvoice.self).filter(predicate).sorted(byKeyPath: "date", ascending: false)
        return results.filter{ $0.hasDailyInvoices(businessName: businessName) }.sorted{ $0.date > $1.date }
    }
    
    /// Load monthlyInvoice of the given day if it exists, other
    static func loadMonthlyInvoice(of date: Date = Date()) -> MonthlyInvoice? {
        guard let realm = try? Realm() else { return nil }
        // prepare Predicate for filtering
        guard let startDate = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: date))),
            let endDate = Calendar.current.date(byAdding: .month, value: 1, to: startDate)
            else { return nil }
        let predicate = NSPredicate(format: "date >= %@ AND date < %@", startDate as NSDate, endDate as NSDate)
        // query Realm
        if let monthlyInvoice = realm.objects(MonthlyInvoice.self).filter(predicate).first {
            return monthlyInvoice
        } else {
            let monthlyInvoice = MonthlyInvoice()
            monthlyInvoice.date = date
            do {
                try realm.write {
                    realm.add(monthlyInvoice)
                }
            } catch {
                AnalyticsUtil.trackError(error)
                return nil
            }
            return monthlyInvoice
        }
    }
    
    /// Load daily invoice of today if it exists, otherwise, create it
    static func loadDailyInvoice(of date: Date = Date()) -> DailyInvoice? {
        guard let monthlyInvoice = PersistanceUtil.loadMonthlyInvoice(of: date) else { return nil }
        guard let realm = try? Realm() else { return nil }
        // prepare Predicate for filtering
        let startDate = Calendar.current.startOfDay(for: date)
        guard let endDate = Calendar.current.date(byAdding: .day, value: 1, to: startDate) else { return nil }
        let predicate = NSPredicate(format: "date >= %@ AND date < %@", startDate as NSDate, endDate as NSDate)
        // query Realm
        if let dailyInvoice = realm.objects(DailyInvoice.self).filter(predicate).first {
            return dailyInvoice
        } else {
            let dailyInvoice = DailyInvoice()
            dailyInvoice.date = date
            do {
                try realm.write {
                    realm.add(dailyInvoice)
                    monthlyInvoice.dailyInvoices.append(dailyInvoice)
                }
            } catch {
                AnalyticsUtil.trackError(error)
                return nil
            }
            return dailyInvoice
        }
    }
    
    /// Save image into a new invoice
    static func saveInvoice(of date: Date,
                            thumbnailURL: URL? = nil,
                            imageURL: URL,
                            category: InvoiceCategory,
                            totalAmount: Double = 0.0,
                            gstAmount: Double = 0.0,
                            abn: String?,
                            note: String?) -> Bool {
        guard let dailyInvoice = loadDailyInvoice(of: date) else { return false }
        let invoice = Invoice()
        invoice.invoiceDate = date
        invoice.category = category
        invoice.totalAmountIncludeGst = totalAmount
        invoice.gst = gstAmount
        invoice.abn = abn
        invoice.note = note
        invoice.sequenceInTheDay = dailyInvoice.nextSequenceInTheDay
        invoice.localThumbnailURLAbsoluteString = thumbnailURL?.absoluteString
        invoice.localImageURLAbsoluteString = imageURL.absoluteString
        invoice.localThumbnailRelativePath = thumbnailURL?.fileHomeDirectoryRelatedPath
        invoice.localImageRelativePath = imageURL.fileHomeDirectoryRelatedPath
        invoice.businessName = loadUser().business?.name ?? ""
        do {
            let realm = try Realm()
            try realm.write {
                // save invoice
                realm.add(invoice)
                // update dailyInvoice
                dailyInvoice.invoices.append(invoice)
                dailyInvoice.pendingUploadedInvoices.append(invoice)
            }
        } catch {
            AnalyticsUtil.trackError(error)
            return false
        }
        updateInvoiceByTextRecogniztionIfPossible(invoice)
        return true
    }
    
    /// Extract Text from invoice image and update the invoice
    static func updateInvoiceByTextRecogniztionIfPossible(_ invoice: Invoice) {
        guard let imagePath = invoice.localImageRelativePath, let image = FileUtil.loadImage(from: imagePath) else { return }
        TextRecognitionUtil.recognizeTextFrom(image) { text in
            do {
                let realm = try Realm()
                try realm.write {
                    invoice.fullContent = text
                    /// detect ABN if it's not set by user
                    if invoice.abn != nil {
                        invoice.abn = invoice.fullContent?.abn
                    }
                }
            } catch {
                AnalyticsUtil.trackError(error)
            }
        }
    }
    
    /// Delete invoice
    static func deleteInvoice(_ invoice: Invoice) -> Bool {
        guard let imagePath = invoice.localImageRelativePath,
            let thumbnailPath = invoice.localThumbnailRelativePath
            else { return false }
        FileUtil.deleteFile(from: imagePath)
        FileUtil.deleteFile(from: thumbnailPath)
        do {
            let realm = try Realm()
            try realm.write {
                // save invoice
                realm.delete(invoice)
            }
        } catch {
            AnalyticsUtil.trackError(error)
            return false
        }
        return true
    }
    
    /// Update invoice
    static func updateInvoice(_ invoice: Invoice,
                              invoiceDate: Date? = nil,
                              invoiceCategory: InvoiceCategory? = nil,
                              totalAmount: Double = 0.0,
                              gstAmount: Double = 0.0,
                              abn: String?,
                              note: String?) -> Bool {
        // update related DailyInvoice if needed
        if let oldInvoiceDate = invoice.invoiceDate,
            let newInvoiceDate = invoiceDate,
            oldInvoiceDate != newInvoiceDate {
            // remove from old DailyInvoice
            if let oldDailyInvoice = loadDailyInvoice(of: oldInvoiceDate),
                let index = oldDailyInvoice.invoices.index(of: invoice) {
                do {
                    let realm = try Realm()
                    try realm.write {
                        oldDailyInvoice.invoices.remove(at: index)
                        if oldDailyInvoice.invoices.isEmpty {
                            realm.delete(oldDailyInvoice)
                        }
                    }
                } catch {
                    AnalyticsUtil.trackError(error)
                }
            }
            // add into new DailyInvoice
            if let newDailyInvoice = loadDailyInvoice(of: newInvoiceDate) {
                do {
                    let realm = try Realm()
                    try realm.write {
                        newDailyInvoice.invoices.append(invoice)
                    }
                } catch {
                    AnalyticsUtil.trackError(error)
                }
            }
        }
        
        // update Invoice itself
        do {
            let realm = try Realm()
            try realm.write {
                if let invoiceDate = invoiceDate {
                    invoice.invoiceDate = invoiceDate
                }
                if let invoiceCategory = invoiceCategory {
                    invoice.category = invoiceCategory
                }
                invoice.totalAmountIncludeGst = totalAmount
                invoice.gst = gstAmount
                invoice.abn = abn
                invoice.note = note
            }
        } catch {
            AnalyticsUtil.trackError(error)
            return false
        }
        return true
    }
    
    /// Load all Invoices which belong to the current business
    static func loadInvoices() -> [Invoice] {
        guard let realm = try? Realm(), let businessName = loadUser().business?.name else { return [] }
        let predicate = NSPredicate(format: "businessName == %@", businessName)
        let results = realm.objects(Invoice.self).filter(predicate).sorted(byKeyPath: "invoiceDate", ascending: false)
        return results.map{ $0 }
    }
    
    // MARK: - User
    
    /// Load current user, if doesn't exist, create one
    static func loadUser() -> User {
        let realm = try! Realm()
        // query Realm
        if let user = realm.objects(User.self).first {
            return user
        } else {
            let user = User()
            do {
                try realm.write {
                    realm.add(user)
                }
            } catch {
                AnalyticsUtil.trackError(error)
            }
            return user
        }
    }
    
    /// Append businesses to user
    static func updateUser(_ user: User, with businessNames: [String]) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                for name in businessNames {
                    user.businesses.append(Business(name: name))
                }
            }
        } catch {
            AnalyticsUtil.trackError(error)
            return false
        }
        return true
    }
    
    static func updateUser(_ user: User, with selectedBusiness: Business) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                user.business = selectedBusiness
            }
        } catch {
            AnalyticsUtil.trackError(error)
            return false
        }
        return true
    }
    
    /// delete businesses from user
    static func delete(_ business: Business) -> Bool {
        do {
            let realm = try Realm()
            try realm.write {
                realm.delete(business)
            }
        } catch {
            AnalyticsUtil.trackError(error)
            return false
        }
        return true
    }

}
