//
//  PickPhotoViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 7/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

class PickPhotoViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var previewView: CameraPreviewView!
    @IBOutlet weak var capturedImageView: UIImageView!
    @IBOutlet weak var buttonsContainerView: UIView!
    @IBOutlet weak var shutterButton: IMRoundButton!
    @IBOutlet weak var cancelButton: IMRoundButton!
    @IBOutlet weak var settingButton: IMRoundButton!
    @IBOutlet weak var confirmButton: IMRoundButton!
    @IBOutlet weak var confirmButtonCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButtonCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var propertiesView: UIView!
    @IBOutlet weak var invoiceDateLabel: UILabel!
    @IBOutlet weak var invoiceCategoryTagView: TagView!
    
//    @IBOutlet weak var imageView: UIImageView!
//    @IBOutlet weak var detectedTextLabel: UILabel!
    
    // MARK: - Vars
    
    private let viewModel = CameraViewModel()
    
    /// capturing photo or showing photo
    private var state: CameraViewController.State = .camera {
        didSet {
//            let backView: UIView = state == .camera ? capturedImageView : previewView
//            view.sendSubviewToBack(backView)
            propertiesView.isHidden = state == .camera
        }
    }
    
    // MARK: - Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Clear previous values
        viewModel.totalAmount = 0.0
        viewModel.gstAmount = 0.0
        updatePropertiesView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resetUI()
    }
    
    /// Reset UI to the default values
    private func resetUI() {
        // reset invoice date to be today whenever back to this screen
        UserDefaults.defaultInvoiceDate = Date()
        // cancel uncompleted photo if there is any
        if state == .image {
            tappedCancelButton()
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func tappedPickPhoto() {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func tappedShutterButton() {
        tappedPickPhoto()
    }
    
    @IBAction func tappedCancelButton() {
        toggleButtons()
        capturedImageView.image = nil
        state = .camera
    }
    
    @IBAction func tappedSettingButton() {
    }
    
    @IBAction func tappedConfirmButton() {
        viewModel.image = capturedImageView.image
        viewModel.saveInvoiceIfPossible()
        capturedImageView.image = nil
        toggleButtons()
        state = .camera
    }
    
    @IBAction func tappedImage(_ sender: UITapGestureRecognizer) {
        tappedShutterButton()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let settingViewController = segue.destination as? SettingViewController {
            settingViewController.delegate = self
            settingViewController.viewModel.totalAmount = viewModel.totalAmount
            settingViewController.viewModel.gstAmount = viewModel.gstAmount
            settingViewController.viewModel.abn = viewModel.abn
            settingViewController.viewModel.note = viewModel.note
        }
    }
    
    // MARK: - Buttons
    
    /// Show either shutter button to capture invoice or the other buttons to cancel/set/confirm
    private func toggleButtons() {
        shutterButton.isHidden = state == .camera ? true : false
        cancelButton.isHidden = state == .camera ? false : true
        settingButton.isHidden = state == .camera ? false : true
        confirmButton.isHidden = state == .camera ? false : true
        buttonsContainerView.bringSubviewToFront(state == .camera ? settingButton : shutterButton)
        let offset = state == .camera ? (buttonsContainerView.frame.width / 2 + shutterButton.frame.width / 2) / 2 : 0
        confirmButtonCenterXConstraint.constant = offset
        if state == .camera {
            cancelButtonCenterXConstraint.constant = -offset
            UIView.animate(withDuration: 0.3) { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.buttonsContainerView.layoutIfNeeded()
            }
        } else {
            cancelButtonCenterXConstraint.constant = offset
        }
        state = state == .camera ? .image : .camera
    }
    
    // MARK: - Functions
    
    private func updatePropertiesView() {
        invoiceDateLabel.text = viewModel.invoiceDate.formattedDateMonthYearString
        invoiceCategoryTagView.invoiceCategory = viewModel.invoiceCategory
    }
    
}

// MARK: - Image picker
extension PickPhotoViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        capturedImageView.image = (info[.editedImage] as? UIImage) ?? info[.originalImage] as? UIImage
        if let image = capturedImageView.image {
            TextRecognitionUtil.recognizeTextFrom(image) { [weak self] (text) in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.abn = text.abn
                let totalWithGST = text.totalWithGST
                strongSelf.viewModel.totalAmount = totalWithGST.total
                strongSelf.viewModel.gstAmount = totalWithGST.gst
            }
        }
        picker.dismiss(animated: true, completion: { self.toggleButtons() })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension PickPhotoViewController: UINavigationControllerDelegate {
}

// MARK: - SettingDelegate : Handle invoice defaults
extension PickPhotoViewController: SettingDelegate {
    func invoiceDefaultsDidUpdate(invoiceDate: Date, invoiceCategory: InvoiceCategory, totalAmount: Double, gstAmount: Double, abn: String?, note: String?) {
        UserDefaults.defaultInvoiceDate = invoiceDate
        UserDefaults.defaultCategory = invoiceCategory
        viewModel.totalAmount = totalAmount
        viewModel.gstAmount = gstAmount
        viewModel.abn = abn
        viewModel.note = note
        updatePropertiesView()
    }
}
