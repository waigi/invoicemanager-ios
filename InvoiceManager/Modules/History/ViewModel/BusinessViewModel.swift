//
//  BusinessViewModel.swift
//  InvoiceManager
//
//  Created by Can Zhan on 24/12/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation

class BusinessViewModel {
    
    private struct CellIdentifier {
        static let business = "businessCell"
        static let noResult = "noResultCell"
    }
    
    lazy var user = PersistanceUtil.loadUser()
    
    // MARK: Table View
    
    var isMaxBusinessesReached: Bool {
        return user.businesses.count >= 5
    }
    
    var isUserSelectedABusiness: Bool {
        return user.business != nil
    }
    
    var hasBusiness: Bool {
        return user.hasBusiness
    }
    
    var businesses: [Business] {
        return user.businesses.map{ $0 }
    }
    
    var numberOfRows: Int {
        return hasBusiness ? businesses.count : 1
    }
    
    func cellIdentifierAt(_ indexPath: IndexPath) -> String {
        return hasBusiness ? CellIdentifier.business : CellIdentifier.noResult
    }
    
    func cellViewModelAt(_ indexPath: IndexPath) -> BusinessCellViewModel? {
        return hasBusiness ? BusinessCellViewModel(business: businesses[indexPath.row]) : nil
    }
    
    func deleteCellAt(_ indexPath: IndexPath, completionHandler: () -> Void) {
        if PersistanceUtil.delete(businesses[indexPath.row]) {
            completionHandler()
        }
    }
    
    func indexPathOf(_ business: Business) -> IndexPath? {
        for (i, b) in businesses.enumerated() {
            if b.name == business.name {
                return IndexPath(row: i, section: 0)
            }
        }
        return nil
    }
    
    // MARK: Function
    
    func addBusiness(_ name: String, completionHandler: () -> Void) {
        PersistanceUtil.updateUser(user, with: [name])
        completionHandler()
    }
}
