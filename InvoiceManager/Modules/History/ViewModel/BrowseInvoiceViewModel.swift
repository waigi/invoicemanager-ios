//
//  BrowseInvoiceViewModel.swift
//  InvoiceManager
//
//  Created by Can Zhan on 30/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation

class BrowseInvoiceViewModel {
    
    // MARK: - Vars
    private var monthlyInvoices = PersistanceUtil.loadAllMonthlyInvoices() {
        didSet {
            filteredInvoices = monthlyInvoices.filter{ $0.hasDailyInvoices(businessName: User.selectedBusinessName) }
        }
    }
    
    private var filteredInvoices = [MonthlyInvoice]()
    
    // MARK: - Functions
    
    func refreshData() {
        filteredInvoices = PersistanceUtil.loadAllMonthlyInvoices()
    }
    
    // MARK: TableView data source
    
    var numberOfSections: Int { return filteredInvoices.count }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        return filteredInvoices[section].filteredDailyInvoices(businessName: User.selectedBusinessName).count
    }
    
    func viewModelAtIndexPath(_ indexPath: IndexPath, with invoiceSelectionHandler: @escaping ((Invoice) -> Void)) -> DailyInvoiceTableViewCellViewModel {
        let dailyInvoice = filteredInvoices[indexPath.section].filteredDailyInvoices(businessName: User.selectedBusinessName)[indexPath.row]
        return DailyInvoiceTableViewCellViewModel(dailyInvoice: dailyInvoice, invoiceSelectionHandler: invoiceSelectionHandler)
    }
    
}
