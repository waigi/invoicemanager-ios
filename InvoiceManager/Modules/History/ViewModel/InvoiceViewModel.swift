//
//  InvoiceViewModel.swift
//  InvoiceManager
//
//  Created by Can Zhan on 9/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation

struct InvoiceViewModel {
    
    let invoice: Invoice
    
    // MARK: - Computed Vars
    
    var formattedDateString: String {
        return invoice.invoiceDate?.formattedDateMonthYearString ?? ""
    }
    
    /// Delete current viewing invoice
    func delete() -> Bool {
        return PersistanceUtil.deleteInvoice(invoice)
    }
    
}
