//
//  BusinessViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 20/12/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

protocol BusinessDelegate: class {
    func didSelectBusiness(business: Business)
}

class BusinessViewController: UIViewController {
    
    weak var delegate: BusinessDelegate?
    
    private let viewModel = BusinessViewModel()

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    private func setupUI() {
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        refreshUIData()
    }
    
    private func refreshUIData() {
        tableView.reloadData()
        // preselect
        if let business = PersistanceUtil.loadUser().business,
            let indexPath = viewModel.indexPathOf(business) {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
        }
    }

    @IBAction func tappedCloseButton() {
        if viewModel.hasBusiness {
            if !viewModel.isUserSelectedABusiness,
                let selectedBusiness = viewModel.businesses.first {
                // select the first business for user if user hasn't selected any, applicable when the first time user adds a business
                selectBusinessAndDismiss(business: selectedBusiness)
            }
            dismiss(animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "No Business Selected", message: "Please add and select at least one business to start.", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.tappedAddButton()
            })
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func tappedAddButton() {
        guard !viewModel.isMaxBusinessesReached else {
            let alertController = UIAlertController(title: "Max number of business reached", message: "You have reached maximum number of businesses. Please upgrade if you need to add more.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
            return
        }
        let alertController = UIAlertController(title: "Add new business", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "Business Name"
            textField.returnKeyType = .done
        }
        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { [weak self] alert -> Void in
            guard let strongSelf = self else { return }
            if let businessName = alertController.textFields?[0].text?.trimmingCharacters(in: .whitespacesAndNewlines), !businessName.isEmpty {
                strongSelf.viewModel.addBusiness(businessName, completionHandler: {
                    strongSelf.refreshUIData()
                })
            }
        })
        
        alertController.addAction(saveAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Functions
    
    private func selectBusinessAndDismiss(business: Business) {
        if PersistanceUtil.updateUser(PersistanceUtil.loadUser(), with: business) {
            delegate?.didSelectBusiness(business: business)
            dismiss(animated: true, completion: nil)
        } else {
            // TODO: show alert?
        }
    }
}

// MARK: - UITableViewDataSource
extension BusinessViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifierAt(indexPath), for: indexPath)
        if let cell = cell as? BusinessCell {
            cell.viewModel = viewModel.cellViewModelAt(indexPath)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension BusinessViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return viewModel.hasBusiness
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.deleteCellAt(indexPath) {
                tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard viewModel.hasBusiness else { return }
        let selectedBusiness = viewModel.businesses[indexPath.row]
        selectBusinessAndDismiss(business: selectedBusiness)
    }
}
