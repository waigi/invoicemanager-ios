//
//  ExportViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 2/1/19.
//  Copyright © 2019 Can Zhan. All rights reserved.
//

import UIKit
import MessageUI

class ExportViewController: UIViewController {
    
    @IBOutlet weak var exportStatusLabel: UILabel!
    @IBOutlet weak var exportProgressView: UIProgressView!
    @IBOutlet weak var downloadLinkLabel: UILabel!
    @IBOutlet weak var downloadView: UIView!
    @IBOutlet weak var emailLinkButton: IMButton!
    
    private var temporaryFileToBeCleaned: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        exportProgressView.progress = 0
        downloadLinkLabel.text = nil
        downloadView.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        exportInvoices()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        FileUtil.deleteFile(from: temporaryFileToBeCleaned)
        WebServer.stop()
    }
    
    // MARK: - Email
    @IBAction func tappedEmailButton() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            if let serverURLString = downloadLinkLabel.text {
                mail.setSubject("Exported Invoices Download Link")
                let string = """
                    <html>
                        <body>
                            <a href="\(serverURLString)">Click me to download invoices.</a>
                        </body>
                    </html>
                    """
                mail.setMessageBody(string, isHTML: true)
            }
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    @IBAction func tappedCloseButton() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Export and Launch Web Server
    private func exportInvoices() {
        FileUtil.exportAndZipInvoices(
            progress: { [weak self] (progress: Double) in
                guard let strongSelf = self else { return }
                strongSelf.exportProgressView.progress = Float(progress)
            },
            success: { [weak self] (zipFileURL) in
                guard let strongSelf = self else { return }
                strongSelf.temporaryFileToBeCleaned = zipFileURL
                strongSelf.exportStatusLabel.text = "1. Invoices exported successfully."
                strongSelf.downloadView.isHidden = false
                // web server
                WebServer.filePath = zipFileURL.path
                if let serverURL = WebServer.start() {
                    downloadLinkLabel.text = serverURL.description
                    emailLinkButton.isEnabled = true
                } else {
                    downloadLinkLabel.text = "Failed to launch the webserver, please make sure your device is connected to WiFi. If yes, try again later or reboot your device."
                }
            },
            failure: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.downloadLinkLabel.text = "Failed to export invoices, please try again later or reboot your device."
            })
    }

}

extension ExportViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
