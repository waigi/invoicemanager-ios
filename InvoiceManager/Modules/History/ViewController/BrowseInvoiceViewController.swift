    //
//  BrowseInvoiceViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 30/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

class BrowseInvoiceViewController: UIViewController {
    
    private struct Identifier {
        static let showInvoiceSegue = "showInvoiceDetails"
        static let dailyInvoiceTableViewCell = "DailyInvoiceTableViewCell"
    }

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel = BrowseInvoiceViewModel()
    private var tappedInvoice: Invoice?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = 210
        tableView.tableFooterView = UIView()
        // navigation bar
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = .cyan50
        navigationController?.navigationBar.tintColor = .cyan900
        // Don't show back title on Invoice screen
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // tab bar
        tabBarController?.tabBar.isHidden = false
        // reset invoice date to be today whenever back to this screen
        UserDefaults.defaultInvoiceDate = Date()
        viewModel.refreshData()
        refreshUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !PersistanceUtil.loadUser().hasBusiness {
            showBusinesses()
        }
    }
    
    private func refreshUI() {
        navigationItem.title = PersistanceUtil.loadUser().business?.name
        tableView.reloadData()
    }

    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? InvoiceDetailsViewController,
            let tappedInvoice = tappedInvoice {
            vc.viewModel = InvoiceViewModel(invoice: tappedInvoice)
        }
    }
    
    // MARK: - IBActions
    @IBAction func tappedChangeButton() {
        showBusinesses()
    }
    
    // MARK: - Function
    private func showBusinesses() {
        if let viewController = R.storyboard.history.businessViewController() {
            present(viewController, animated: true, completion: nil)
        }
    }
}

// MARK: - UITableViewDataSource
extension BrowseInvoiceViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.dailyInvoiceTableViewCell, for: indexPath)
        if let cell = cell as? DailyInvoiceTableViewCell {
            cell.viewModel = viewModel.viewModelAtIndexPath(indexPath, with: { [weak self] (invoice) in
                guard let strongSelf = self else { return }
                strongSelf.tappedInvoice = invoice
                strongSelf.performSegue(withIdentifier: Identifier.showInvoiceSegue, sender: strongSelf)
            })
        }
        return cell
    }
}

extension BrowseInvoiceViewController: BusinessDelegate {
    func didSelectBusiness(business: Business) {
        viewModel.refreshData()
        refreshUI()
    }
}
