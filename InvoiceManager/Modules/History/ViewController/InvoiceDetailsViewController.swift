//
//  InvoiceDetailsViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 13/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

/// Invoice text content
class InvoiceDetailsViewController: UIViewController {

    private struct Identifier {
        static let showInvoiceSegue = "showInvoice"
    }
    
    // MARK: - IBOutlets
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var invoiceDateLabel: UILabel!
    @IBOutlet weak var invoiceCategoryTagView: TagView!
    @IBOutlet weak var financialYearLabel: UILabel!
    @IBOutlet weak var detectedContentView: RoundCornerView!
    @IBOutlet weak var abnLabel: UILabel!
    @IBOutlet weak var gstLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    
    // MARK: - Vars
    var viewModel: InvoiceViewModel?
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // nav bar
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        // image tapped action
        thumbnailImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(InvoiceDetailsViewController.tappedInvoiceImage)))
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // tab bar
        tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: - UI
    
    private func setupUI() {
        detectedContentView.applyShadow()
        thumbnailImageView.applyBorder(borderColor: .cyan50)
        noteLabel.applyBorder(borderColor: .cyan50)
        refreshData()
    }
    
    private func refreshData() {
        if let path = viewModel?.invoice.localImageRelativePath {
            thumbnailImageView.image = FileUtil.loadImage(from: path)
        }
        invoiceDateLabel.text = viewModel?.formattedDateString
        if let category = viewModel?.invoice.category {
            invoiceCategoryTagView.invoiceCategory = category
        }
        financialYearLabel.text = viewModel?.invoice.invoiceDate?.financialYearString
        abnLabel.text = viewModel?.invoice.abn
        gstLabel.text = viewModel?.invoice.gst.rawDollarString
        totalAmountLabel.text = viewModel?.invoice.totalAmountIncludeGst.rawDollarString
        noteLabel.text = viewModel?.invoice.note ?? viewModel?.invoice.fullContent
    }
    
    // MARK: - IBActions
    
    @IBAction func tappedDeleteButton(_ sender: UIBarButtonItem) {
        let message = NSLocalizedString("Delete this invoice?", comment: "Pop up before deleting")
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        let yesAction = UIAlertAction(title: "Yes", style: .destructive) { [weak self] (_) in
            guard let strongSelf = self else { return }
            if let isDeleted = strongSelf.viewModel?.delete(), isDeleted {
                strongSelf.navigationController?.popViewController(animated: true)
            } else {
                let message = NSLocalizedString("Failed to delete this invoice, please try again later.", comment: "Error message when failed to delete")
                let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                strongSelf.present(alertController, animated: true, completion: nil)
            }
        }
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        present(alertController, animated: true, completion: nil)
    }

    @IBAction func tappedCancelButton() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func tappedInvoiceImage() {
        performSegue(withIdentifier: Identifier.showInvoiceSegue, sender: self)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SettingViewController,
            let invoiceDate = viewModel?.invoice.invoiceDate,
            let invoiceCategory = viewModel?.invoice.category,
            let totalAmount = viewModel?.invoice.totalAmountIncludeGst,
            let gstAmount = viewModel?.invoice.gst
        {
            vc.delegate = self
            vc.viewModel.invoiceDate = invoiceDate
            vc.viewModel.invoiceCategory = invoiceCategory
            vc.viewModel.totalAmount = totalAmount
            vc.viewModel.gstAmount = gstAmount
            vc.viewModel.abn = viewModel?.invoice.abn
            vc.viewModel.note = viewModel?.invoice.note
        } else if let vc = segue.destination as? InvoiceViewController {
            vc.viewModel = viewModel
        }
    }

}

// MARK: - SettingDelegate : Handle invoice defaults
extension InvoiceDetailsViewController: SettingDelegate {
    func invoiceDefaultsDidUpdate(invoiceDate: Date, invoiceCategory: InvoiceCategory, totalAmount: Double, gstAmount: Double, abn: String?, note: String?) {
        guard let invoice = viewModel?.invoice else { return }
        if !PersistanceUtil.updateInvoice(invoice, invoiceDate: invoiceDate, invoiceCategory: invoiceCategory, totalAmount: totalAmount, gstAmount: gstAmount, abn: abn, note: note) {
            print("Failed to update invoice.")
        }
        refreshData()
    }
}
