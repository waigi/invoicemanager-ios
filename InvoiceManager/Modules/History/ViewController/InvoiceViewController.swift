//
//  InvoiceViewController.swift
//  InvoiceManager
//
//  Created by Can Zhan on 9/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

/// Invoice image
class InvoiceViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: - Vars
    var viewModel: InvoiceViewModel?

    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let path = viewModel?.invoice.localImageRelativePath {
            imageView.image = FileUtil.loadImage(from: path)
        }
        // Double tap gesture
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(InvoiceViewController.doubleTappedImage))
        doubleTapGesture.numberOfTapsRequired = 2
        imageView.addGestureRecognizer(doubleTapGesture)
        // scroll view
        updateMinZoomScaleForSize(scrollView.bounds.size)
        doubleTappedImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // tab bar
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateMinZoomScaleForSize(scrollView.bounds.size)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    // MARK: - IBAction
    
    @IBAction func pinchedImage(_ sender: UIPinchGestureRecognizer) {
        if sender.state == .changed {
        }
    }
    
    
    @objc private func doubleTappedImage() {
        if (scrollView.zoomScale > scrollView.minimumZoomScale) {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }
    
    @IBAction func tappedViewDetails(_ sender: UIBarButtonItem) {
        
    }
    
    // MARK: - UI

    private func updateMinZoomScaleForSize(_ size: CGSize) {
        guard let image = imageView.image else { return }
        let widthScale = size.width / image.size.width
        let heightScale = size.height / image.size.height
        let minScale = min(widthScale, heightScale)
        scrollView.minimumZoomScale = min(widthScale, heightScale)
        scrollView.zoomScale = minScale
    }
    
    private func updateConstraintsForSize(_ size: CGSize) {
        
        let yOffset = max(0, (size.height - imageView.frame.height) / 2)
        imageViewTopConstraint.constant = yOffset
        imageViewBottomConstraint.constant = yOffset
        
        let xOffset = max(0, (size.width - imageView.frame.width) / 2)
        imageViewLeadingConstraint.constant = xOffset
        imageViewTrailingConstraint.constant = xOffset
        
        view.layoutIfNeeded()
    }
}

extension InvoiceViewController: UIScrollViewDelegate {
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        updateConstraintsForSize(scrollView.bounds.size)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
