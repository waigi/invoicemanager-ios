//
//  DailyInvoiceCollectionViewCell.swift
//  InvoiceManager
//
//  Created by Can Zhan on 30/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

class DailyInvoiceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var categoryTagView: TagView!
    
    var viewModel: InvoiceCollectionViewCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            imageView.image = viewModel.invoiceImage
            categoryTagView.invoiceCategory = viewModel.invoice.category
        }
    }
}

class InvoiceCollectionViewCellViewModel: NSObject {
    
    let invoice: Invoice
    
    init(invoice: Invoice) {
        self.invoice = invoice
    }
    
    var invoiceImage: UIImage? {
        guard let filePath = invoice.localThumbnailRelativePath else { return nil }
        return FileUtil.loadImage(from: filePath)
    }
    
    var categoryString: String {
        return invoice.categoryString
    }
}
