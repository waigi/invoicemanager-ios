//
//  DailyInvoiceTableViewCell.swift
//  InvoiceManager
//
//  Created by Can Zhan on 30/10/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

class DailyInvoiceTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var invoiceCountLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var viewModel: DailyInvoiceTableViewCellViewModel? {
        didSet {
            guard let viewModel = viewModel else { return }
            dateLabel.text = viewModel.dateString
            invoiceCountLabel.text = viewModel.invoiceCountString
            collectionView.dataSource = viewModel
            collectionView.delegate = viewModel
            collectionView.reloadData()
        }
    }
}

class DailyInvoiceTableViewCellViewModel: NSObject {
    
    let dailyInvoice: DailyInvoice
    /// Perform action in main queue when an invoice is tapped
    var invoiceSelectionHandler: ((Invoice) -> Void)
    
    init(dailyInvoice: DailyInvoice, invoiceSelectionHandler: @escaping ((Invoice) -> Void)) {
        self.dailyInvoice = dailyInvoice
        self.invoiceSelectionHandler = invoiceSelectionHandler
    }
    
    var dateString: String {
        return dailyInvoice.date.formattedDateMonthYearString
    }
    
    var invoiceCountString: String {
        return "(\(dailyInvoice.filteredInvoices(businessName: User.selectedBusinessName).count) \(dailyInvoice.filteredInvoices(businessName: User.selectedBusinessName).count == 1 ? "invoice" : "invoices"))"
    }
}

// MAKR: - Collection view to show invoice images
extension DailyInvoiceTableViewCellViewModel: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dailyInvoice.filteredInvoices(businessName: User.selectedBusinessName).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DailyInvoiceCollectionViewCell", for: indexPath)
        if let cell = cell as? DailyInvoiceCollectionViewCell {
            let invoice = dailyInvoice.filteredInvoices(businessName: User.selectedBusinessName)[indexPath.row]
            cell.viewModel = InvoiceCollectionViewCellViewModel(invoice: invoice)
        }
        return cell
    }
}

extension DailyInvoiceTableViewCellViewModel: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            if let cell = collectionView.cellForItem(at: indexPath) as? DailyInvoiceCollectionViewCell,
                let invoice = cell.viewModel?.invoice {
                strongSelf.invoiceSelectionHandler(invoice)
            }
        }
    }
}
