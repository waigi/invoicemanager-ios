//
//  TagView.swift
//  InvoiceManager
//
//  Created by Can Zhan on 1/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

@IBDesignable class TagView: UIView {
    
    let label = UILabel(frame: .zero)
    
    var invoiceCategory: InvoiceCategory = .cashPayment {
        didSet {
            label.text = invoiceCategory.description
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        // border
        applyBorder(borderWidth: 0)
        // background
        let backgroundColor: UIColor
        switch invoiceCategory {
        case .cashPayment: backgroundColor = .backgroundBlue
        case .eftPayment: backgroundColor = .backgroundYellow
        }
        self.backgroundColor = backgroundColor
        // label
        label.text = invoiceCategory.description
        label.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        label.textColor = UIColor.cyan900
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        label.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        label.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4).isActive = true
        label.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4).isActive = true
        label.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
        label.setContentHuggingPriority(.required, for: .horizontal)
        label.setContentHuggingPriority(.required, for: .vertical)
        layoutIfNeeded()
    }
}
