//
//  BusinessCell.swift
//  InvoiceManager
//
//  Created by Can Zhan on 20/12/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit

class BusinessCell: UITableViewCell {

    @IBOutlet weak var businessNameLabel: UILabel!
    
    var viewModel: BusinessCellViewModel? {
        didSet {
            if let viewModel = viewModel {
                businessNameLabel.text = viewModel.business.name
            }
        }
    }
}

struct BusinessCellViewModel {
    let business: Business
}

class NoResultCell: UITableViewCell {
    
}
