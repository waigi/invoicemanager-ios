//
//  Regex.swift
//  InvoiceManager
//
//  Created by Can Zhan on 8/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation

struct RegexPattern {
    
    /// Pattern: (a|A)\W*(b|B)\W*(n|N)(\D*)(\d\s*){11}
    /// Find ABC string with any case of ABN with it's 11 digits and any number of spaces among the the whole matching string.
    /// e.g. "ABN 440 23 123 123", "abn23453829463", "A b n 39 234 234 234", "A b n no 12 213 123 123", "A. b. n.: 12 213 123 123" are all matching strings.
    static let abn = "(a|A)\\W*(b|B)\\W*(n|N)(\\D*)(\\d\\s*){11}"
    
    /// Pattern: (\d){11}
    /// Check if a string is a valid ABN
    static let validABN = "(\\d){11}"
    
    /// Pattern: ([0-9]*,*)*\.[0-9]{2}
    /// Match amount like: 12.00, 123,456.78
    static let moneyAmount = "([0-9]*,*)*\\.[0-9]{2}"
}
