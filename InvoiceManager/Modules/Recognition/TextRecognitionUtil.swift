//
//  TextRecognitionUtil.swift
//  InvoiceManager
//
//  Created by Can Zhan on 6/11/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import UIKit
import Firebase

struct TextRecognitionUtil {
    
    /// Recognize text from image via Google ML
    static func recognizeTextFrom(_ image: UIImage, completion: @escaping (String) -> Void) {
        let vision = Vision.vision()
        let textRecognizer = vision.onDeviceTextRecognizer()
        let image = VisionImage(image: image.renderResizedImage(newWidth: 1080))
        
        textRecognizer.process(image) { (visionText, error) in
            guard error == nil, let text = visionText else {
                print(error)
                return
            }
            completion(text.text)
        }
    }
    
}
