//
//  AnalyticsUtil.swift
//  InvoiceManager
//
//  Created by Can Zhan on 18/12/18.
//  Copyright © 2018 Can Zhan. All rights reserved.
//

import Foundation
import Crashlytics

/// Providing static functions to track either business logics via Omniture or errors via Crashlytics
struct AnalyticsUtil {
    
    /// initial context data such as username, user profile, etc.
    private static var contextData: [String: Any] {
        let user = PersistanceUtil.loadUser()
        let data: [String: Any] = ["username": user.username]
        Crashlytics.sharedInstance().setUserIdentifier(user.username)
        return data
    }
    
    /// Context data (username, user profile) with file name, function name, and line number.
    private static func contextDataWith(file: String, function: String, line: Int) -> [String: Any] {
        var contextData = AnalyticsUtil.contextData
        contextData["error.file"] = file.components(separatedBy: "/").last
        contextData["error.function"] = function
        contextData["error.line"] = line
        return contextData
    }
    
    // MARK: - Tracking Functions with Firebase
    
    /// Track screen state with Firebase, This is a convinent function to track using enum rather than string of screen name
    static func trackScreen(_ screen: Screen, data: [AnyHashable : Any]? = nil) {
        trackScreen(screen.rawValue, data: data)
    }
    
    /// Track screen state with Firebase, This is a convinent function to track using enum rather than string of screen name
    static func trackAction(_ action: Action, data: [AnyHashable : Any]? = nil) {
        trackAction(action.rawValue, data: data)
    }
    
    /// Track screen state with Firebase
    static func trackScreen(_ screenName: String, data: [AnyHashable : Any]? = nil) {
    }
    
    /// Track App action with Firebase
    static func trackAction(_ actionName: String, data: [AnyHashable : Any]? = nil) {
    }
    
    // MARK: - Tracking Errors with Crashlytics
    
    /// Track common Error
    static func trackError(_ error: Error, userInfo: [AnyHashable : Any]? = nil, file: String = #file, function: String = #function, line: Int = #line) {
        var contextData = AnalyticsUtil.contextDataWith(file: file, function: function, line: line)
        contextData["error.userInfo"] = userInfo
        Crashlytics.sharedInstance().recordError(error, withAdditionalUserInfo: contextData)
    }
    
    /// Track DecodingError(e.g. failed to parse JSON)
    static func trackDecodingError(_ error: Error, file: String = #file, function: String = #function, line: Int = #line) {
        var contextData = AnalyticsUtil.contextDataWith(file: file, function: function, line: line)
        let log: String
        if let decodingError = error as? DecodingError {
            switch decodingError {
            case .dataCorrupted(let context), .keyNotFound(_, let context), .typeMismatch(_, let context), .valueNotFound(_, let context):
                log = context.debugDescription
            }
        } else {
            log = "The error which is supposed to be a DecodingError, is actually not."
        }
//        contextData["decodingerror.code"] = error.code
        contextData["decodingerror.log"] = log
        Crashlytics.sharedInstance().recordError(error, withAdditionalUserInfo: contextData)
    }
    
}
// MARK: - enum of all screens to be tracked
extension AnalyticsUtil {
    /// Screens to be tracked
    enum Screen: String {
        case welcome = "Welcome Screen"
        case launch = "Launch Screen"
        case tnc = "Terms & Conditions Screen"
        case login = "Login Screen"
    }
    
    /// Actions to be tracked
    enum Action: String {
        // login
        case loginLogin = "Login"
        case loginForget = "Forgot Password"
    }
}
